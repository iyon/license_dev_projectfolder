import { Component } from '@angular/core';
import { dataTablePdf1 } from '../data/data-pdf';
import { MatDialog } from '@angular/material';
import { Mom } from '../mom-nodin/mom/mom';


@Component({
  selector: 'test-component',
  templateUrl: 'test.component.html',
  styles: [""]  
})
export class TestComponent {  
    items;
    items2: string[];
    dateList;
    
  constructor(public dialog: MatDialog) {
    this.dateList = [1,2,3,4,5,6]
    this.items = dataTablePdf1;
  }   

  // openDialogMom(){
  //   let dialogRef = this.dialog.open(Mom,{
  //     width: '100%',
  //     height: '100%'
  //   })
  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
  //   });
  // }
}
