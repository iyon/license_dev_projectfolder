import {Component, OnInit, AfterViewInit, Input} from "@angular/core";
import { AmChartsService } from "@amcharts/amcharts3-angular";

@Component({
  selector: 'chart-dynamicas',
  template: `

  <div class="chartdiv" [id]="id"></div>

  `,
  styles: [`
  .chartdiv {
    width   : 100%;
    height  : 130px;
}   
  `]
})
export class ChartDynamic implements OnInit, AfterViewInit {
    @Input() id;

    constructor(private amchartService: AmChartsService){
    
    }
    
    ngOnInit() {
        
    }

    ngAfterViewInit(){
        // console.log("id", this.id)
        this.createChart2(this.id)
    }

    createChart(id){        
        var chartData = generateChartData();        
        let say = this.amchartService
        var chart = say.makeChart( id, {
        "type": "serial",
        "theme": "light",
        "dataProvider": chartData,
        "valueAxes": [ {
            "position": "left",
            "title": "Unique visitors"
        } ],
        "graphs": [ {
            "valueField": "visits"
        } ],
        "categoryField": "date",
        "categoryAxis": {
            "minPeriod": "mm",
            "parseDates": true
        }
        } );
        
        // generate some random data, quite different range
        function generateChartData() {
        var chartData = [];
        // current date
        var firstDate = new Date();
        // now set 500 minutes back
        firstDate.setMinutes( firstDate.getDate() - 200 );
        
        // and generate 500 data items
        for ( var i = 0; i < 200; i++ ) {
            var newDate = new Date( firstDate );
            // each time we add one minute
            newDate.setMinutes( newDate.getMinutes() + i );
            // some random number
            var visits = Math.round( Math.random() * 40 + 100 );
            // add data item to the array
            chartData.push( {
            date: newDate,
            visits: visits
            } );
        }
        return chartData;
        }
        
        /**
         * An interval to add new data points
         */
        var timeout;
        // setInterval( function() {
        // // normally you would load new data points via AJAX
        // // for the sake of this demo we will just generate random values
        
        // // remove one data point from beginning
        // chart.dataProvider.shift();
        
        // // add new data point to the end
        // var newDate = new Date( chart.dataProvider[ chart.dataProvider.length - 1 ].date );
        // // each time we add one minute
        // newDate.setMinutes( newDate.getMinutes() + 1 );
        // // some random number
        // var visits = Math.round( Math.random() * 40 + 100 );
        // // add data item to the array
        // chart.dataProvider.push( {
        //     date: newDate,
        //     visits: visits
        // } );
            
        // if (timeout)
        //     clearTimeout(timeout);
        // timeout = setTimeout(function () {
        //     chart.validateData();
        // });
        // }, 100 );
    }

    createChart2(id){        
        var chartData = generateChartData();        
        let say = this.amchartService
        var chart = say.makeChart( id, {
        "type": "serial",
        "theme": "dark",
        "marginLeft": 0,
        "dataProvider": chartData,
        "balloon": {
            "cornerRadius": 6,
            "horizontalPadding": 15,
            "verticalPadding": 10
        },
        "valueAxes": [ {
            "position": "left",
            "title": "",
            "labelsEnabled": false
        } ],
        "graphs": [ {
            "bullet": "square",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            // "fillAlphas": 0.3,
            // "fillColorsField": "lineColor",
            "legendValueText": "duration: [[value]]",
            "lineColorField": "lineColor",
            "title": "duration",
            "valueField": "duration"
        },
        {
            "bullet": "triangleUp",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            // "fillAlphas": 0.3,
            "legendValueText": "[[value]]",
            "lineColor": "#FF6600",
            "title": "duration2",
            "valueField": "duration2"
        }],
        "categoryField": "date",
        "categoryAxis": {
            // "dateFormats": [{
            //     "period": "DD",
            //     "format": "DD"
            // }, {
            //     "period": "WW",
            //     "format": "MMM DD"
            // }, {
            //     "period": "MM",
            //     "format": "MMM"
            // }, {
            //     "period": "YYYY",
            //     "format": "YYYY"
            // }],
            "minPeriod": "mm",
            "parseDates": true
        },
        "chartCursor": {
            "categoryBalloonDateFormat": "YYYY MMM DD",
            "cursorAlpha": 0,
            "fullWidth": true
        }
        } );
        
        // generate some random data, quite different range
        function generateChartData() {
            return [{
                "lineColor": "#b7e021",
                "date": "2012-01-01",
                "duration": 408,
                "duration2": 300
            }, {
                "date": "2012-01-02",
                "duration": 482,
                "duration2": 350
            }, {
                "date": "2012-01-03",
                "duration": 562,
                "duration2": 200
            }, {
                "date": "2012-01-04",
                "duration": 379,
                "duration2": 300
            }, {
                "lineColor": "#fbd51a",
                "date": "2012-01-05",
                "duration": 501
            }, {
                "date": "2012-01-06",
                "duration": 443
            }, {
                "date": "2012-01-07",
                "duration": 405
            }, {
                "date": "2012-01-08",
                "duration": 309,
                "lineColor": "#2498d2"
            }, {
                "date": "2012-01-09",
                "duration": 287
            }, {
                "date": "2012-01-10",
                "duration": 485
            }, {
                "date": "2012-01-11",
                "duration": 890
            }, {
                "date": "2012-01-12",
                "duration": 810
            }]
        }
    }
}