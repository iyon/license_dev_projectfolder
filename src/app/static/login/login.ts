import { Component, OnInit, ViewChild, AfterContentInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import 'ag-grid-enterprise';
import { NgForm } from '@angular/forms';
import { first } from 'rxjs/operators';


@Component({
  selector: 'login',
  template: `
  <form #f="ngForm" (ngSubmit)="onSubmit(f)" novalidate>
  <input name="username" ngModel required #first="ngModel">
  <input name="password" ngModel>
  <button>Submit</button>
</form>

<mdb-carousel [isControls]="true" class="carousel slide carousel-fade" [animation]="'fade'">
    <!--First slide-->
    <mdb-carousel-item>
        <div class="view w-100">
            <div>asdfasdf</div>
            <div class="mask rgba-black-light waves-light" mdbWavesEffect></div>
        </div>
        <div class="carousel-caption">
            <h3 class="h3-responsive">Light mask</h3>
            <p>First text</p>
        </div>
    </mdb-carousel-item>
    <!--/First slide-->
    <!--Second slide-->
    <mdb-carousel-item>
        <div class="view w-100">
            <div>asdfdsf</div>
          <div class="mask rgba-black-strong waves-light" mdbWavesEffect></div>
        </div>
        <div class="carousel-caption">
            <h3 class="h3-responsive">Strong mask</h3>
            <p>Secondary text</p>
        </div>
    </mdb-carousel-item>
    <!--/Second slide-->
    <!--Third slide-->
    <mdb-carousel-item>
        <div class="view w-100">
        <div>telu</div>
            <div class="mask rgba-black-slight waves-light" mdbWavesEffect></div>
        </div>
        <div class="carousel-caption">
            <h3 class="h3-responsive">Slight mask</h3>
            <p>Third text</p>
        </div>
    </mdb-carousel-item>
    <!--/Third slide-->
</mdb-carousel>
<!--/.Carousel Wrapper-->
  `,
  styles: [""]  
})
export class LoginComponent {  
  first;
  constructor() {
    
  }    

  onSubmit(f: NgForm) {
    var isValid = f.valid
    var username = f.value.username
    var password = f.value.password
    console.log("first",this.first);
  }
}
