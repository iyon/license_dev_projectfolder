export const dataTablePdf1 = [
    {
    "date": "2018-10-31",
    "onair_2g": 0,
    "offair_2g": 607,
    "total_2g": 62687,
    "onair_3g": 0,
    "offair_3g": 2881,
    "total_3g": 98178,
    "onair_4g": 0,
    "offair_4g": 115,
    "total_4g": 51841,
    "onair_trx": 0,
    "offair_trx": 4673,
    "total_trx": 814158,
    "total_all": 212706,
    "only_2g": 5562,
    "only_3g": 1511,
    "only_4g": 486,
    "collocation": 38260,
    "total_all_site": 45819
    },
    {
    "date": "2018-09-01",
    "onair_2g": 0,
    "offair_2g": 607,
    "total_2g": 62687,
    "onair_3g": 0,
    "offair_3g": 2881,
    "total_3g": 98178,
    "onair_4g": 0,
    "offair_4g": 115,
    "total_4g": 51841,
    "onair_trx": 0,
    "offair_trx": 4673,
    "total_trx": 814158,
    "total_all": 212706,
    "only_2g": 5562,
    "only_3g": 1511,
    "only_4g": 486,
    "collocation": 38260,
    "total_all_site": 0
    },
    {
    "date": "2018-08-01",
    "onair_2g": 0,
    "offair_2g": 607,
    "total_2g": 62687,
    "onair_3g": 0,
    "offair_3g": 2881,
    "total_3g": 98178,
    "onair_4g": 0,
    "offair_4g": 115,
    "total_4g": 51841,
    "onair_trx": 0,
    "offair_trx": 4673,
    "total_trx": 814158,
    "total_all": 212706,
    "only_2g": 5562,
    "only_3g": 1511,
    "only_4g": 486,
    "collocation": 38260,
    "total_all_site": 0
    },
    {
    "date": "2018-07-01",
    "onair_2g": 0,
    "offair_2g": 607,
    "total_2g": 62687,
    "onair_3g": 0,
    "offair_3g": 2881,
    "total_3g": 98178,
    "onair_4g": 0,
    "offair_4g": 115,
    "total_4g": 51841,
    "onair_trx": 0,
    "offair_trx": 4673,
    "total_trx": 814158,
    "total_all": 212706,
    "only_2g": 5562,
    "only_3g": 1511,
    "only_4g": 486,
    "collocation": 38260,
    "total_all_site": 0
    },
    {
    "date": "2018-06-01",
    "onair_2g": 0,
    "offair_2g": 607,
    "total_2g": 62687,
    "onair_3g": 0,
    "offair_3g": 2881,
    "total_3g": 98178,
    "onair_4g": 0,
    "offair_4g": 115,
    "total_4g": 51841,
    "onair_trx": 0,
    "offair_trx": 4673,
    "total_trx": 814158,
    "total_all": 212706,
    "only_2g": 5562,
    "only_3g": 1511,
    "only_4g": 486,
    "collocation": 38260,
    "total_all_site": 0
    },
    {
    "date": "2018-05-01",
    "onair_2g": 0,
    "offair_2g": 607,
    "total_2g": 62687,
    "onair_3g": 0,
    "offair_3g": 2881,
    "total_3g": 98178,
    "onair_4g": 0,
    "offair_4g": 115,
    "total_4g": 51841,
    "onair_trx": 0,
    "offair_trx": 4673,
    "total_trx": 814158,
    "total_all": 212706,
    "only_2g": 5562,
    "only_3g": 1511,
    "only_4g": 486,
    "collocation": 38260,
    "total_all_site": 0
    }
    ]

// export const dataTablePdf1 = [
//     {"title": "BTS 2G - On Air this year","data1": "-","data2": "-","data3": "-","data4": "-","data5": "-","data6": "-"},
//     {"title": "BTS 2G - Dismantled","data1": "1","data2": "2","data3": "3","data4": "-","data5": "-","data6": "-"},
//     {"title": "BTS 2G -Total On Air","data1": "50,319","data2": "50,317","data3": "50,315","data4": "50,315","data5": "50,315","data6": "50,315"},
//     {"title": "BTS 3G - On this year","data1": "-","data2": "-","data3": "-","data4": "-","data5": "-","data6": "-"},
//     {"title": "BTS 3G - Dismantled","data1": "-","data2": "2","data3": "3","data4": "-","data5": "-","data6": "-"},
//     {"title": "BTS 3G - Total On Air","data1": "82,224","data2": "82,222 ","data3": "82,219","data4": "82,219","data5": "82,219 ","data6": "82,219"},
//     {"title": "BTS 4G - On this year","data1": "707","data2": "4,830","data3": "3,304 ","data4": "2,591","data5": "2,146 ","data6": "4,467"},
//     {"title": "BTS 4G - Dismantled ","data1": "-","data2": "4","data3": "1","data4": "1","data5": "-","data6": "-"},
//     {"title": "BTS 4G - Total On Air","data1": "30,284","data2": "35,110","data3": "38,413","data4": "41,003","data5": "43,149","data6": "47,616"},
//     {"title": "TRX - On Air this year","data1": "-","data2": "-","data3": "-","data4": "-","data5": "-","data6": "-"},
//     {"title": "TRX - Dismantled","data1": "4,577","data2": "3,662 ","data3": "2,013","data4": "1,756","data5": "232 ","data6": "1,590"},
//     {"title": "TRX - Total On Air","data1": "594,793","data2": "591,131 ","data3": "589,118 ","data4": "587,362","data5": "587,130 ","data6": "585,540"},
//     {"title": "Jumlah BTS (2G, 3G & 4G)","data1": "162,827","data2": "167,649","data3": "170,947","data4": "173,537","data5": "175,683","data6": "180,150"},    
//     {"title": "Jumlah Site 2G Only","data1": "6,700 ","data2": "6,460 ","data3": "6,209","data4": "5,949 ","data5": "5,694","data6": "5,362"},
//     {"title": "Jumlah Site 3G Only ","data1": "8,623 ","data2": "6,873 ","data3": "6,054 ","data4": "5,439","data5": "4,882","data6": "3,877"},
//     {"title": "Jumlah Site 4G Only","data1": "1,096 ","data2": "1,221 ","data3": "1,350","data4": "1,537 ","data5": "1,748 ","data6": "1,983"},
//     {"title": "Jumlah Site 2G 3G & 4G","data1": "37,079","data2": "39,065","data3": "40,171 ","data4": "41,061 ","data5": "41,873 ","data6": "43,206"},
//     {"title": "Total Jumlah Site ","data1": "53,498","data2": "53,619","data3": "53,784","data4": "53,986","data5": "54,197","data6": "54,428"}  
// ]

export const dataTablePdf2 = [
    {"no": "1", "regional": "Regional1", "data1":"57", "data2":"5986", "data3":"35", "data4":"6250", "data5":"3878"},
    {"no": "2", "regional": "Regional2", "data1":"34", "data2":"4842", "data3":"19", "data4":"6516", "data5":"3794"},
    {"no": "3", "regional": "Regional3", "data1":"28", "data2":"6579", "data3":"33", "data4":"17090", "data5":"10933"},
    {"no": "4", "regional": "Regional4", "data1":"26", "data2":"3692", "data3":"15", "data4":"7615", "data5":"4438"},
    {"no": "5", "regional": "Regional5", "data1":"37", "data2":"3810", "data3":"20", "data4":"6208", "data5":"4885"},
    {"no": "6", "regional": "Regional6", "data1":"42", "data2":"4340", "data3":"24", "data4":"8279", "data5":"4958"},
    {"no": "7", "regional": "Regional7", "data1":"51", "data2":"3280", "data3":"37", "data4":"4755", "data5":"2689"},
    {"no": "8", "regional": "Regional8", "data1":"55", "data2":"4898", "data3":"47", "data4":"8405", "data5":"3947"},
    {"no": "9", "regional": "Regional9", "data1":"79", "data2":"6610", "data3":"48", "data4":"9413", "data5":"4238"},
    {"no": "10", "regional": "Regional10", "data1":"54", "data2":"4803", "data3":"40", "data4":"5236", "data5":"2963"},
    {"no": "11", "regional": "Regional11", "data1":"35", "data2":"1475", "data3":"26", "data4":"2452", "data5":"893"}
]

export const dataTablePdf3 = [
    {"region":"R3 Jabotabek ", "data1": "BOO751", "data2": "DESAPASIRANGINMG", "data3": "LTE1800", "data4": "Huawei", "data5": " Takeout, achievment Mei 2018"},
    {"region":"R3 Jabotabek ", "data1": "JSX386", "data2": "PLZOLEOSTOWERMACML", "data3": "LTE1800-INDOOR ", "data4": "Huawei", "data5": " Takeout, achievment Mei 2018"},
    {"region":"R3 Jabotabek ", "data1": "TNG468", "data2": "LAMPSITEITCBSD", "data3": "LTE1800-INDOOR ", "data4": "Huawei", "data5": " Takeout, achievment Mei 2018"}
]

export const tembusan = [
    {"no":"1", "person":"Director of Network"},
    {"no":"2", "person":"Director of Finance"},
    {"no":"3", "person":"VP Network Planning & Engineering"},
    {"no":"4", "person":"VP Network Operation Management"},
    {"no":"5", "person":"VP Network Deployment and Services"},
    {"no":"6", "person":"GM IOC Management"},
    {"no":"7", "person":"GM RAN and Core Network Deployment"},
    {"no":"8", "person":"GM Network Integrated Program and Budgeting"}
]

export const tembusan2 = [
    {"no":"1", "person":"Director of Network"},
    {"no":"2", "person":"Director of Finance"},
    {"no":"3", "person":"General Manager IOC Management"},
    {"no":"4", "person":"General Manager Network Integrated Program and Budgeting"}
]

export const listPerson = [
    {"name": "Agus Witjaksono", "title":"Vice President Network Deployment and Services"},
    {"name": "Mustaghfirin", "title":"Vice President Network Planning and Engineering"},
    {"name": "Andrias Indra", "title":"Vice President Network Operation Management"},
    {"name": "Djoko S Kuntjoro", "title":"Vice President Network Infrastructure Management"},
    {"name": "Chairuddin1009", "title":"General Manager Network Asset Management"},
    {"name": "Cecep Riswanda", "title":"General Manager RAN and Core Network Deployment"},
    {"name": "Suharno", "title":"General Manager Radio Network Planning"},
    {"name": "Iswandi", "title":"General Manager RAN Operation"}
]    

export const headerPdf = 
    {
        "nomor": "002/TC.02/NM-07/NL-11/NG-03/NJ-04/NL-01/NG-01/NJ-01/NM-01/VIII/2018", 
        "kepada":["Vice President Financial Planning Analysis and Business Partner"], 
        "dari":[
            "Vice President Network Deployment and Services",
            "Vice President Network Planning and Engineering",
            "Vice President Network Operation Management",
            "Vice President Network Infrastructure Management",
            "General Manager Network Asset Management"
        ],
        "lampiran":"1 berkas",
        "perihal":"Pelaporan BTS On-air Telkomsel Juli 2018 (Periode 1- 31 Juli 2018)"
    }

    export const tes = [
        {
            "date_str":"dd/mm/yy", 
            "2g":"", 
            "3g":"", 
            "4g":"", 
            "total":"",
            "child":[
                {"region":"", "2g":"", "3g":"", "4g":"", "total":""},
                {"region":"", "2g":"", "3g":"", "4g":"", "total":""},
            ]
        },
        {
            "date_str":"dd/mm/yy", 
            "2g":"", 
            "3g":"", 
            "4g":"", 
            "total":"",
            "child":[
                {"region":"", "2g":"", "3g":"", "4g":"", "total":""},
                {"region":"", "2g":"", "3g":"", "4g":"", "total":""},
            ]
        }
    ]