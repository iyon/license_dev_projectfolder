import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { FeaturesComponent } from './features/features.component';
import { LicenseComponent } from './license/license.component';
import { LoginComponent } from './login/login';
import { Nodin } from './mom-nodin/nodin/nodin';
import { Mom } from './mom-nodin/mom/mom';
import { TestComponent } from './test/testing.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'login' }
  },
  {
    path: 'dashboard-license',
    component: LicenseComponent,
    data: { title: 'NEISA | LICENSE' }
  },
  {
    path: 'nodin',
    component: Nodin,
    data: { title: 'Nodin' }
  },
  {
    path: 'momies',
    component: Mom,
    data: { title: 'Mom' }
  }
  // ,
  // {
  //   path: 'test',
  //   component: TestComponent,
  //   data: { title: 'Test Component' }
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaticRoutingModule {}
