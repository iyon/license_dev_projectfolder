import {Component, Input} from "@angular/core"; 

@Component({
  selector: 'paragraph-pdf',
  template: `
    <table>
      <tr>
        <td>
          {{head}}
        </td>
        <td rowspan="2">
          {{text}}
        </td>
      </tr>
      <tr>
        <td></td>
      </tr>
    </table>
  `,
  styles: [`
    td{
      height: 20px;
      font-weight: 400;
      font-size: 1rem;
      vertical-align: baseline;
    }
  `] 
})
export class Paragraph {
  @Input() head;
  @Input() text;
}