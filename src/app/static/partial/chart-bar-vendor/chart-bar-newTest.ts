import {Component, OnInit, AfterViewInit, Input} from "@angular/core";
import { AmChartsService } from "@amcharts/amcharts3-angular";

@Component({
  selector: 'chart-bar-newTest',
  template: `  
  <div style="color: white;margin-left: 10px;"></div>  
  <div style="width:200%;;height:130px;margin-top:-15px;" [id]="id"></div>
  

  <!-- <a style="color: white;margin-left: 10px;">{{vendor}}</a>  
  <div class="chartdiv" [id]="id"></div> -->
  `,
  styles: [`
  .chartdiv {
	width		: 100%;
	height		: 100px;
	font-size	: 11px;
  }
  `]
})
export class ChartBarNewTest implements AfterViewInit {
    @Input() id;
    // @Input() vendor;
    @Input() data;

    constructor(private amchartService: AmChartsService){
    
    }

    ngAfterViewInit(){
        // console.log("this id", this.id)
        // console.log("this data", this.data)
        this.createChart(this.id, this.data)
    }

    createChart(id, data){
      // console.log("id", id)
      //   console.log("data", data)
        let say = this.amchartService
        var chart = say.makeChart(id, {
          "type": "serial",
          "theme": "none",
          "dataProvider": [{
              "name": "John",
              "points": 35654,
              "color": "#7F8DA9",
              // "bullet": "https://www.amcharts.com/lib/images/faces/C02.png",
              "customDescription": "35654",
          }, {
              "name": "Damon",
              "points": 65456,
              "color": "#FEC514",
              // "bullet": "https://www.amcharts.com/lib/images/faces/C02.png",
              "customDescription": "65456",
          }, {
              "name": "Patrick",
              "points": 45724,
              "color": "#DB4C3C",
              // "bullet": "https://www.amcharts.com/lib/images/faces/D02.png",
              "customDescription": "45724",
          }, {
              "name": "Mark",
              "points": 13654,
              "color": "#DAF0FD",
              // "bullet": "https://www.amcharts.com/lib/images/faces/E01.png",
              "customDescription": "13654",
          }],
          "valueAxes": [{
              "maximum": 80000,
              "minimum": 0,
              "axisAlpha": 0,
              "dashLength": 4,
              "position": "left"
          }],
          "startDuration": 1,
          "graphs": [{
              "balloonText": "<span style='font-size:13px;'>[[category]]: <b>[[value]]</b></span>",
              "bulletOffset": 10,
              "bulletSize": 52,
              "colorField": "color",
              "cornerRadiusTop": 8,
              "customBulletField": "bullet",
              "fillAlphas": 0.8,
              "lineAlpha": 0,
              "type": "column",
              "valueField": "points",
              "labelText": "[[customDescription]]",
              "labelColorField": "#fff !important",
          }],
          "marginTop": 0,
          "marginRight": 0,
          "marginLeft": 0,
          "marginBottom": 0,
          "autoMargins": false,
          "categoryField": "name",
          "categoryAxis": {
              "axisAlpha": 0,
              "gridAlpha": 0,
              "inside": true,
              "tickLength": 0
          },
          "export": {
            "enabled": true
           }
          
          } );
    }
}
