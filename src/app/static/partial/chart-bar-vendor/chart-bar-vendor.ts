import {Component, OnInit, AfterViewInit, Input} from "@angular/core";
import { AmChartsService } from "@amcharts/amcharts3-angular";

@Component({
  selector: 'chart-bar-vendor',
  template: `  
  <a style="color: white;margin-left: 10px;">{{vendor}}</a>  
  <div class="chartdiv" [id]="id"></div>
  `,
  styles: [`
  .chartdiv {
	width		: 100%;
	height		: 100px;
	font-size	: 11px;
  }
  `]
})
export class ChartBarVendor implements AfterViewInit {
    @Input() id;
    @Input() vendor;
    @Input() data;

    constructor(private amchartService: AmChartsService){
    
    }

    ngAfterViewInit(){
        // console.log("id", this.id)
        this.createChart(this.id, this.data)
    }

    createChart(id, data){
        let say = this.amchartService
        var chart = say.makeChart(id, {
            "type": "serial",
            "theme": "light",
            "dataProvider": data,
            "valueAxes": [{
              "gridColor": "#FFFFFF",
              "gridAlpha": 0.2,
              "dashLength": 1,
              "labelsEnabled": false    
            }],
            // "gridAboveGraphs": true,
            // "startDuration": 1,
            "graphs": [ {
              "balloonText": "Allocated: <b>[[value]]</b>",
              "fillAlphas": 0.9,
              "lineAlpha": 0.1,
              "type": "column",
              "valueField": "allocated",
              "fillColorsField": "#ffffff"
            },{
              "balloonText": "Used: <b>[[value]]</b>",
              "fillAlphas": 0.9,
              "lineAlpha": 0.1,
              "type": "column",
              "valueField": "used",
              "fillColorsField": "#ff8d00"
            } ],
            "chartCursor": {
              "categoryBalloonEnabled": false,
              "cursorAlpha": 0,
              "zoomable": false
            },
            "categoryField": "license",
            "categoryAxis": {
              "color": "#efefef",
              "gridPosition": "start",
              // "labelRotation": 45
            },
            // "categoryAxis": {
            //   "gridPosition": "start",
            //   "gridAlpha": 0,
            //   "tickPosition": "start",
            //   "tickLength": 20
            // },
            "export": {
              "enabled": false
            }
          
          } );
    }
}
