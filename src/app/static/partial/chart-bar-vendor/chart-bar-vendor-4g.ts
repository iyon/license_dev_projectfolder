import {Component, OnInit, AfterViewInit, Input} from "@angular/core";
import { AmChartsService } from "@amcharts/amcharts3-angular";


@Component({
  selector: 'chart-bar-vendor-4g',
  template: `  
  <div style="color: white;margin-left: 10px;">{{vendor}}</div>  
  <div class="chartdiv" [id]="id"></div>
  `,
  styles: [`
  .chartdiv {
	width		: 100%;
	height		: 130px;
    font-size	: 11px;
    margin-top  : -15px;
  }
  `]
})
export class ChartBarVendor4G implements AfterViewInit {
    @Input() id;
    @Input() vendor;
    @Input() data;

    constructor(private amchartService: AmChartsService){
    
    }

    ngAfterViewInit(){
        console.log("id", this.id)
        this.createChart(this.id, this.data)
    }

    // createChart(id, data){
    //     let say = this.amchartService;
    //     var chart = say.makeChart(id, {
    //         "theme": "none",
    //         "type": "serial",
    //         "dataProvider": [{
    //             "country": "USA",
    //             "year2004": 3.5,
    //             "year2005": 4.2
    //         }, {
    //             "country": "UK",
    //             "year2004": 1.7,
    //             "year2005": 3.1
    //         }, {
    //             "country": "Canada",
    //             "year2004": 2.8,
    //             "year2005": 2.9
    //         }, {
    //             "country": "Japan",
    //             "year2004": 2.6,
    //             "year2005": 2.3
    //         }, {
    //             "country": "France",
    //             "year2004": 1.4,
    //             "year2005": 2.1
    //         }, {
    //             "country": "Brazil",
    //             "year2004": 2.6,
    //             "year2005": 4.9
    //         }],
    //         "valueAxes": [{
    //             // "unit": "%",
    //             // "position": "left",
    //             // "title": "GDP growth rate",
    //             "labelsEnabled": false
    //         }],
    //         "startDuration": 1,
    //         "graphs": [{
    //             "balloonText": "GDP grow in [[category]] (2004): <b>[[value]]</b>",
    //             "fillAlphas": 0.9,
    //             "lineAlpha": 0.2,
    //             "title": "2004",
    //             "type": "column",
    //             "valueField": "year2004",
    //             "fillColors": "#ffffff"
    //         }, {
    //             "balloonText": "GDP grow in [[category]] (2005): <b>[[value]]</b>",
    //             "fillAlphas": 0.9,
    //             "lineAlpha": 0.2,
    //             "title": "2005",
    //             "type": "column",
    //             "clustered":false,
    //             "columnWidth":0.5,
    //             "valueField": "year2005",
    //             "fillColors": "rgb(103, 148, 220)"
    //         }],
    //         "plotAreaFillAlphas": 0.1,
    //         "categoryField": "country",
    //         "categoryAxis": {
    //             "gridPosition": "start"
    //         },
    //         "export": {
    //             "enabled": false
    //          }
        
    //     });
    // }

    createChart(id, data){
        let say = this.amchartService;
        var chart = say.makeChart(id, {
            "theme": "none",
            "type": "serial",
            // "dataProvider": [{
            //     "license": "Cell FDD",
            //     "used": 3.5,
            //     "allocated": 4.2
            // }, {
            //     "license": "Cell TTD",
            //     "used": 1.7,
            //     "allocated": 3.1
            // }, {
            //     "license": "FDD Band",
            //     "used": 2.8,
            //     "allocated": 2.9
            // }, {
            //     "license": "TTD Band",
            //     "used": 2.6,
            //     "allocated": 2.3
            // }, {
            //     "license": "TTD Power",
            //     "used": 1.4,
            //     "allocated": 2.1
            // }, {
            //     "license": "FDD Power",
            //     "used": 2.6,
            //     "allocated": 4.9
            // }],
            "dataProvider": data,
            "valueAxes": [{
                // "unit": "%",
                // "position": "left",
                // "title": "GDP growth rate",
                "labelsEnabled": false
            }],
            "startDuration": 1,
            "graphs": [{
                "balloonText": "[[category]] : <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "Allocated",
                "type": "column",
                "valueField": "allocated",
                "fillColors": "#ffffff"
            }, {
                "balloonText": "[[category]] : <b>[[value]]</b>",
                "fillAlphas": 0.9,
                "lineAlpha": 0.2,
                "title": "Used",
                "type": "column",
                "clustered":false,
                "columnWidth":0.5,
                "valueField": "used",
                "fillColors": "rgb(103, 148, 220)"
            }],
            "plotAreaFillAlphas": 0.1,
            "categoryField": "license",
            "categoryAxis": {
                "gridPosition": "start",
                // "labelRotation": 45
            },
            "export": {
                "enabled": false
             }
        
        });
    }
}
