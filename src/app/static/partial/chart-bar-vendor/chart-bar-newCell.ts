import {Component, OnInit, AfterViewInit, Input} from "@angular/core";
import { AmChartsService } from "@amcharts/amcharts3-angular";

@Component({
  selector: 'chart-bar-newCell3g',
  template: `  
  <div style="color: white;margin-left: 10px;"></div>  
  <div style="width:200%;;height:165px;margin-top:-15px;" [id]="id"></div>
  

  <!-- <a style="color: white;margin-left: 10px;">{{vendor}}</a>  
  <div class="chartdiv" [id]="id"></div> -->
  `,
  styles: [`
  .chartdiv {
	width		: 100%;
	height		: 100px;
	font-size	: 11px;
  }
  `]
})
export class ChartBarNewCell3G implements AfterViewInit {
    @Input() id;
    // @Input() vendor;
    @Input() data;

    constructor(private amchartService: AmChartsService){
    
    }

    ngAfterViewInit(){
        // console.log("this id", this.id)
        // console.log("this data", this.data)
        this.createChart(this.id, this.data)
    }

    createChart(id, data){
      // console.log("id", id)
      //   console.log("data", data)
        let say = this.amchartService

        // let gradient1 = chart.LinearGradient(0, 0, 0, 400);
        //   // gradient1.addColorStop(0, 'rgba(181, 92, 124, 0.1)'); 
        //   // gradient1.addColorStop(1, 'rgba(181, 92, 124, 1)'); 
        //   gradient1.addColorStop(0, 'rgba(48, 208, 220, .5)');    
        //   gradient1.addColorStop(1, 'rgba(48, 208, 220, 1)');
        // let gradient2 = chart.createLinearGradient(0, 0, 0, 400);
        //   gradient2.addColorStop(0, 'rgba(252, 112, 115, .5)');    
        //   gradient2.addColorStop(1, 'rgba(252, 112, 115, 1)');
        // let gradient3 = chart.createLinearGradient(0, 0, 0, 450);
        //   gradient3.addColorStop(0, 'rgba(151,122,208, .5 )');  
        //   gradient3.addColorStop(1, 'rgba(151,122,208, 1)');

        var chart = say.makeChart(id, {
            "type": "serial",
            "theme": "light",
            "dataProvider": 
            data,
            // [data],
            // [
            //   {
            //   "week": "W5",
            //   "trx_active": 401901,
            //   "trx_unactive": 30780,
            //   "trx_total": 432681
            //   },
            //   {
            //   "week": "W6",
            //   "trx_active": 401640,
            //   "trx_unactive": 26949,
            //   "trx_total": 428589
            //   }
            //   ],
            "valueAxes": [{
              "gridColor": "#FFFFFF",
              "gridAlpha": 0.2,
              // "dashLength": 1,
              "labelsEnabled": false,
              "maximum": Number(data[0].trx_total)+100000,
              "autoGridCount": false,    
            }],
            "titles": [
              {
                "text": "Jumlah Cell",
                "size": 12,
                "color": "#fff",
              }
            ],
            // "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [ 
              {
              "balloonText": "ACTIVE: <b>[[value]]</b>",
              "fillAlphas": 0.9,
              "fillColors": ["rgba(48, 208, 220, 1)", "rgba(48, 208, 220, .5)"],
              "lineAlpha": 0.1,
              "type": "column",
              "valueField": "trx_active",
              "fillColorsField": "#ffffff",
              // "fillColorsField": "linear-gradient(to top, rgba(49, 113, 160,.5), rgba(1, 14, 23,.8));",
              "labelText": "[[active_label]]",
              "color": "#ffffff",
            },{
              "balloonText": "NOT ACTIVE: <b>[[value]]</b>",
              "fillAlphas": 0.9,
              "fillColors": ["rgba(252, 112, 115, 1)", "rgba(252, 112, 115, .5)"],
              "lineAlpha": 0.1,
              "type": "column",
              "valueField": "trx_unactive",
              "fillColorsField": "#ff8d00",
              // "fillColorsField": gradient2,
              "labelText": "[[unactive_label]]",
              "color": "#ffffff",
            },{
              "balloonText": "TOTAL: <b>[[value]]</b>",
              "fillAlphas": 0.9,
              "fillColors": ["rgba(151,122,208, 1)", "rgba(151,122,208, .5 )"],
              "lineAlpha": 0.2,
              // "columnWidth": 0.5,
              "type": "column",
              "valueField": "trx_total",
              "fillColorsField": "#ff8d00",
              // "fillColorsField": gradient3,
              "labelText": "[[total_label]]",
              "color": "#ffffff",
            }  
          ],
            "chartCursor": {
              "categoryBalloonEnabled": false,
              "cursorAlpha": 0,
              "zoomable": false
            },
            "categoryField": "week",
            "categoryAxis": {
              "color": "#efefef",
              "gridPosition": "start",
              // "labelRotation": 45
            },
            // "categoryAxis": {
            //   "gridPosition": "start",
            //   "gridAlpha": 0,
            //   "tickPosition": "start",
            //   "tickLength": 20
            // },
            "export": {
              "enabled": false
            }
          
          } );
    }
}
