import {Component, Input} from "@angular/core";

@Component({
  selector: 'pdf-bag2',
  template: `

    <div>
      <b>{{name}}</b>
      <p>{{title}}</p>
    </div>
    
  `,
  styles: [`
    
        

  `]
})
export class PdfBag2 {
  @Input() name:any;  
  @Input() title:any;  

    constructor() {
        
    }
}