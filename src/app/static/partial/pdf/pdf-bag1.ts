import {Component} from "@angular/core";
import { headerPdf } from "../../data/data-pdf";

@Component({
  selector: 'pdf-bag1',
  template: `

    <div id="pdf-bag1" style="width:100%">
        <div style="width:100%">
            <div class="row">
                <div class="col-1">Nomor</div>
                <div class="col-1">:</div>
                <div class="col-10">{{nomor}}</div>
            </div>
            <div class="row">
                <div class="col-1">Kepada</div>
                <div class="col-1">:</div>
                <div class="col-10">
                    <div *ngFor="let i of kepada">{{i}}</div>
                </div>
            </div>
            <div class="row">
                <div class="col-1">Dari</div>
                <div class="col-1">:</div>
                <div class="col-10">
                    <div *ngFor="let i of dari">{{i}}</div>
                </div>
            </div>
            <div class="row">
                <div class="col-1">Lampiran</div>
                <div class="col-1">:</div>
                <div class="col-10">{{lampiran}}</div>
            </div>
            <div class="row">
                <div class="col-1">Perihal</div>
                <div class="col-1">:</div>
                <div class="col-10"><b>{{perihal}}</b></div>
            </div>
        </div>
    </div>
    
  `,
  styles: [`
    #pdf-bag1{
        color: black;
    }

    .riw{

    }
  `]
})
export class PdfBag1 {
    nomor;
    kepada;
    dari;
    lampiran;
    perihal;

    constructor() {
        let header = headerPdf;
        this.nomor = header.nomor
        this.kepada = header.kepada
        this.dari = header.dari
        this.lampiran = header.lampiran
        this.perihal = header.perihal
    }
}