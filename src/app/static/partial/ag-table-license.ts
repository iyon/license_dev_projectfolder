import { Component, OnInit, ViewChild, Input, Output, EventEmitter   } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment as env } from '@env/environment';
import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core';
import 'ag-grid-enterprise';
import {FeatureService} from '../provider/feature-service';
import { MatDialog } from '@angular/material';
import { LicenseVendorLayer2 } from './dialog/license-vendor-layer2/license-vendor-layer2';
import { LicenseVendorLayer3 } from './dialog/license-vendor-layer2/license-vendor-layer3';
import { LicenseVendorLayer4 } from './dialog/license-vendor-layer2/license-vendor-layer4';
export let types = [];

@Component({
  selector: 'ag-table-license',
  template: `
  <ag-grid-angular
  #agGrid
  style="width: 100%; height: 100%;"
  id="myGrid"
  [rowData]="data"
  class="ag-theme-dark"
  [columnDefs]="colDefs"
  [animateRows]="true"
  [enableSorting]="true"
  [enableFilter]="true"
  [enableColResize]="true"
  [rowSelection]="rowSelection"
  [groupMultiAutoColumn]="true"
  
  (rowClicked)= "onRowClicked($event)"
  (gridReady)="onGridReady($event)"
  [groupDefaultExpanded]="groupDefaultExpanded"
  (firstDataRendered)="onFirstDataRendered($event)"

  
  ></ag-grid-angular>
  
  <!-- 
  (selectionChanged)="onSelectionChanged($event)"
  -->
  `,



  // styleUrls: ['./license.component.scss']
})
export class AgTableLicense{
   gridApi;
   gridColumnApi;
   rowData: any;
   autoGroupColumnDef;
   rowSelection: any;
  groupDefaultExpanded;
  @Input() colDefs:any;
  @Input() data:any;

  

  constructor(private dialog: MatDialog) {
    // console.log("data table", this.data)
    this.autoGroupColumnDef = {
      headerName: " Vendor "
    }    

    this.groupDefaultExpanded = -1;
    this.rowSelection = "single";
  }


  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;  

    setTimeout(function() {
      var rowCount = 0;
      params.api.forEachNode(function(node) {
        node.setExpanded(rowCount++ === 1);
      });
    }, 500);
  }

  // onSelectionChanged(param){
    onRowClicked(param){
    
    var selectedRows = this.gridApi.getSelectedRows();
    // console.log("selectedRows",selectedRows)
    // var selectedNodes = this.gridApi.getSelectedNodes();
    // console.log("selectedNodes",selectedNodes)
    // var getFocusedCell = this.gridApi.getFocusedCell().column.colId
     types = this.gridApi.getFocusedCell().column.colId
    // console.log("getFocusedCell",types)
    if (selectedRows[0].type == "Used"){
      // this.openDialog(selectedRows)
      this.openDialog(selectedRows)
    }
    
  }


  openDialog(data): void {
    let res = data

    // console.log("col table", this.colDefs[2].field)

    if(this.colDefs[2].field == "trx"){

      let dialogRef = this.dialog.open(LicenseVendorLayer2, {
        width: '50%',
        height: '45%',
        data: {
                'dataRes':res
              }
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        // this.animal = result;
      });
    }else if (this.colDefs[2].field == "cell_carrier"){
      let dialogRef = this.dialog.open(LicenseVendorLayer3, {
        width: '50%',
        height: '45%',
        data: {
                'dataRes':res
              }
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        // this.animal = result;
      });
    } else if (this.colDefs[2].field == "cell_fdd"){
      let dialogRef = this.dialog.open(LicenseVendorLayer4, {
        width: '50%',
        height: '45%',
        data: {
                'dataRes':res
              }
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed');
        // this.animal = result;
      });
    }
    
    

    
  }
}