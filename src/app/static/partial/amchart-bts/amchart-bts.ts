import { Component, AfterViewInit, ViewChild,Input, OnInit } from '@angular/core';
import 'ag-grid-enterprise';
import {AmChart, AmChartsService} from '@amcharts/amcharts3-angular';
import { Store } from '@ngrx/store';
import { selectorSettings } from '../../../settings/settings.reducer';
import { FeatureService } from '../../provider/feature-service';

@Component({
  selector: 'amchart-bts',
  templateUrl: './amchart-bts.html',
  styleUrls: ['./amchart-bts.css']
})

export class AmchartBtsComponent implements AfterViewInit, OnInit {
  @Input() id:any;
  chart: any;
  data;
    constructor(private AmCharts: AmChartsService, private store: Store<any>, private featureService: FeatureService) {
       
    }
  
    ngOnInit(){
        
    }

    ngAfterViewInit() {
        this.createChart(this.id)
    }
  
    createChart(id){
        let self = this
        var say = this.AmCharts        
        var chart = say.makeChart(id, {
            "type": "serial",
            "theme": "dark",
            // "responsive": {
            //     "enabled": true
            // },
            // "marginRight": 40,
            "marginLeft": 0,
            "dataProvider": [{
                "lineColor": "#b7e021",
                "date": "2012-01-01",
                "duration": 408
            }, {
                "date": "2012-01-02",
                "duration": 482
            }, {
                "date": "2012-01-03",
                "duration": 562
            }, {
                "date": "2012-01-04",
                "duration": 379
            }, {
                "lineColor": "#fbd51a",
                "date": "2012-01-05",
                "duration": 501
            }, {
                "date": "2012-01-06",
                "duration": 443
            }, {
                "date": "2012-01-07",
                "duration": 405
            }, {
                "date": "2012-01-08",
                "duration": 309,
                "lineColor": "#2498d2"
            }, {
                "date": "2012-01-09",
                "duration": 287
            }, {
                "date": "2012-01-10",
                "duration": 485
            }, {
                "date": "2012-01-11",
                "duration": 890
            }, {
                "date": "2012-01-12",
                "duration": 810
            }],
            "balloon": {
                "cornerRadius": 6,
                "horizontalPadding": 15,
                "verticalPadding": 10
            },
            "valueAxes": [{
                "duration": "mm",
                "durationUnits": {
                    "hh": "h ",
                    "mm": "min"
                },
                "axisAlpha": 0,
                "labelsEnabled": false
            }],
            "graphs": [{
                "bullet": "square",
                "bulletBorderAlpha": 1,
                "bulletBorderThickness": 1,
                "fillAlphas": 0.3,
                "fillColorsField": "lineColor",
                "legendValueText": "[[value]]",
                "lineColorField": "lineColor",
                "title": "duration",
                "valueField": "duration"
            }],
            "chartScrollbar": {
        
            },
            "chartCursor": {
                "categoryBalloonDateFormat": "YYYY MMM DD",
                "cursorAlpha": 0,
                "fullWidth": true
            },
            "dataDateFormat": "YYYY-MM-DD",
            "categoryField": "date",
            "categoryAxis": {
                "dateFormats": [{
                    "period": "DD",
                    "format": "DD"
                }, {
                    "period": "WW",
                    "format": "MMM DD"
                }, {
                    "period": "MM",
                    "format": "MMM"
                }, {
                    "period": "YYYY",
                    "format": "YYYY"
                }],
                "parseDates": true,
                "autoGridCount": false,
                "axisColor": "#555555",
                "gridAlpha": 0,
                "gridCount": 50,
                "labelsEnabled": false
            },
            "export": {
                "enabled": false
            }
            // ,
            // "listeners": [{
            //     "event": "dataUpdated",
            //     "method": function zoomChart(){
            //         chart.zoomToDates(new Date(2012, 0, 3), new Date(2012, 0, 11));
            //     }
            // }]
        });
        
        
        
        chart.addListener("dataUpdated", zoomChart);
        
        function zoomChart() {
            chart.zoomToDates(new Date(2012, 0, 3), new Date(2012, 0, 11));
        }

        // chart.dataProvider = this.data;
        
        // var timeout;
        // setInterval( function() {            
        //     if (timeout)
        //         clearTimeout(timeout);
        //     timeout = setTimeout(function () {
        //         chart.validateData();
        //     });
        //  }, 100 );   
        }
  
  }