import {Component, OnInit, AfterViewInit, Input} from "@angular/core";
import { AmChartsService } from "@amcharts/amcharts3-angular";

@Component({
  selector: 'chart-huawei',
  template: `
  <a style="color: white;margin-left: 10px;">{{vendor}}</a>  
  <div class="chartdiv" [id]="id"></div>

  `,
  styles: [`
  .chartdiv {
    width   : 100%;
    height  : 130px;
}   
  `]
})
export class ChartHuawei implements OnInit, AfterViewInit {
    @Input() id;
    @Input() vendor;

    constructor(private amchartService: AmChartsService){
    
    }
    
    ngOnInit() {
        
    }

    ngAfterViewInit(){
        // console.log("id", this.id)
        this.createChart2(this.id)
    }

    createChart2(id){        
        var chartData = generateChartData();        
        let say = this.amchartService
        var chart = say.makeChart( id, {
        "type": "serial",
        "theme": "dark",
        "marginLeft": 0,
        "dataProvider": chartData,
        // "legend": {
        //     "useGraphSettings": true
        // },
        "balloon": {
            "cornerRadius": 6,
            "horizontalPadding": 15,
            "verticalPadding": 10
        },
        "valueAxes": [ {
            "position": "left",
            "title": "",
            "labelsEnabled": false
        } ],
        "graphs": [
        {
            "bullet": "square",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            // "fillAlphas": 0.3,
            // "fillColorsField": "lineColor",
            // "legendValueText": "[[value]]",
            "lineColor": "#ffffff",
            "title": "trx",
            "valueField": "trx"
        },
        {
            "bullet": "square",
            "bulletBorderAlpha": 1,
            "bulletBorderThickness": 1,
            // "fillAlphas": 0.3,
            // "legendValueText": "[[value]]",
            "lineColor": "#FF6600",
            "title": "power",
            "valueField": "power"
        }],
        "categoryField": "date",
        "categoryAxis": {
            // "dateFormats": [{
            //     "period": "DD",
            //     "format": "DD"
            // }, {
            //     "period": "WW",
            //     "format": "MMM DD"
            // }, {
            //     "period": "MM",
            //     "format": "MMM"
            // }, {
            //     "period": "YYYY",
            //     "format": "YYYY"
            // }],
            "minPeriod": "mm",
            "parseDates": true
        },
        "chartCursor": {
            "categoryBalloonDateFormat": "YYYY MMM DD",
            "cursorAlpha": 0,
            "fullWidth": true
        }
        } );
        
        // generate some random data, quite different range
        function generateChartData() {
            return [{                
                "date": "2012-01-01",
                "trx": 408,
                "power": 300
            }, {
                "date": "2012-01-02",
                "trx": 482,
                "power": 350
            }, {
                "date": "2012-01-03",
                "trx": 562,
                "power": 200
            }, {
                "date": "2012-01-04",
                "trx": 379,
                "power": 300
            }, {
                "date": "2012-01-05",
                "trx": 501,
                "power": 200
            }, {
                "date": "2012-01-06",
                "trx": 443,
                "power": 100
            }, {
                "date": "2012-01-07",
                "trx": 405,
                "power": 600
            }]
        }
    }
}