import { Component, OnInit, ViewChild, Input, Inject  } from '@angular/core';
import 'ag-grid-enterprise';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { types } from '../../ag-table-license';


@Component({
  selector: 'license-vendor-layer2',
  template: `
  <div style="width:100%;height: 100%;color:white;">
    <div style="text-align: left; width: 100%; padding: 2px 0px 0px 15px; color: #fff;height: 15%;background: #008080;border-radius: 5px 5px 0px 0px;">
                    <h5>{{data.dataRes[0].vendor}} {{data.dataRes[0].type}}</h5>

                    <ag-grid-angular
                    #agGrid
                    style="width: 103%;height: 200px;margin-left: -15px;"
                    id="myGrid"
                    [rowData]="rowData"
                    class="ag-theme-dark"
                    [columnDefs]="colDefs"
                    [animateRows]="true"
                    [enableSorting]="true"
                    [enableFilter]="true"
                    [enableColResize]="true"  
                    (gridReady)="onGridReady($event)"
                    (firstDataRendered)="onFirstDataRendered($event)"  
                    ></ag-grid-angular>
  </div>
  `,

  // <ag-license-vendor-l2 [data]="rowData" [colDefs]="colDefs"></ag-license-vendor-l2>


  //<button (click)="closeDialog()"style="font-size: 11px;border-radius: 11px; cursor: pointer;">X</button>
//     <div>Layer2 <button (click)="closeDialog()" style="float: right;font-size: 11px;border-radius: 11px; cursor: pointer;">X</button></div> 



  // styleUrls: ['./license.component.scss']
})

export class LicenseVendorLayer2 implements OnInit {
   colDefs;
   rowData;
   gridApi;
   gridColumnApi;
   token: any;
   vendors: any;
   tipes: any;

  constructor(
    public dialogRef: MatDialogRef<LicenseVendorLayer2>, 
    @Inject(MAT_DIALOG_DATA) public data: any,
    private http: HttpClient) {
      // console.log("data", data)
      
    this.colDefs = [
        {
        "headerName":"LICENSE NAME", 
        "field":"license_name",
        "width":110,
        // "suppressResize":true,
      },
          {
            "headerName":"R-1", 
            "field":"r1",
            cellStyle: function (params) {
               // console.log("params.data", params.data)
              if (params.data !== undefined) {
                // console.log ( "tes",params.dataRes)
                if(params.data.r1.indexOf('-') == -1 ){
                  if(params.data.r1.indexOf('(0)') == -1 ){
                    return {color: '#5aff16'};
                  } else return {color: '#fff'}
                }
                else return {
                  color: '#ff6767'
                };
                }
              },
            // "suppressResize":true,
          },
          {
            "headerName":"R-2", 
            "field":"r2",
            cellStyle: function (params) {
              if (params.data !== undefined) {
                if(params.data.r2.indexOf('-') == -1 ){
                  if(params.data.r2.indexOf('(0)') == -1 ){
                    return {color: '#5aff16'};
                  } else return {color: '#fff'}
                }
                else return {
                  color: '#ff6767'
                };
                }
              },
            // "suppressResize":true,
          },
          {
            "headerName":"R-3", 
            "field":"r3",
            cellStyle: function (params) {
              if (params.data !== undefined) {
                if(params.data.r3.indexOf('-') == -1 ){
                  if(params.data.r3.indexOf('(0)') == -1 ){
                    return {color: '#5aff16'};
                  } else return {color: '#fff'}
                }
                else return {
                  color: '#ff6767'
                };
                }
              },
            // "suppressResize":true,
          },
          {
            "headerName":"R-4", 
            "field":"r4",
            cellStyle: function (params) {
              if (params.data !== undefined) {
                if(params.data.r4.indexOf('-') == -1 ){
                  if(params.data.r4.indexOf('(0)') == -1 ){
                    return {color: '#5aff16'};
                  } else return {color: '#fff'}
                }
                else return {
                  color: '#ff6767'
                };
                }
              },
            // "suppressResize":true,
          },
          {
            "headerName":"R-5", 
            "field":"r5",
            cellStyle: function (params) {
              if (params.data !== undefined) {
                if(params.data.r5.indexOf('-') == -1 ){
                  if(params.data.r5.indexOf('(0)') == -1 ){
                    return {color: '#5aff16'};
                  } else return {color: '#fff'}
                }
                else return {
                  color: '#ff6767'
                };
                }
              },
            // "suppressResize":true,
          },
          {
            "headerName":"R-6", 
            "field":"r6",
            cellStyle: function (params) {
              if (params.data !== undefined) {
                if(params.data.r6.indexOf('-') == -1 ){
                  if(params.data.r6.indexOf('(0)') == -1 ){
                    return {color: '#5aff16'};
                  } else return {color: '#fff'}
                }
                else return {
                  color: '#ff6767'
                };
                }
              },
            // "suppressResize":true,
          },
          {
            "headerName":"R-7", 
            "field":"r7",
            cellStyle: function (params) {
              if (params.data !== undefined) {
                if(params.data.r7.indexOf('-') == -1 ){
                  if(params.data.r7.indexOf('(0)') == -1 ){
                    return {color: '#5aff16'};
                  } else return {color: '#fff'}
                }
                else return {
                  color: '#ff6767'
                };
                }
              },
            // "suppressResize":true,
          },
          {
            "headerName":"R-8", 
            "field":"r8",
            cellStyle: function (params) {
              if (params.data !== undefined) {
                if(params.data.r8.indexOf('-') == -1 ){
                  if(params.data.r8.indexOf('(0)') == -1 ){
                    return {color: '#5aff16'};
                  } else return {color: '#fff'}
                }
                else return {
                  color: '#ff6767'
                };
                }
              },
            // "suppressResize":true,
          },
          {
            "headerName":"R-9", 
            "field":"r9",
            cellStyle: function (params) {
              if (params.data !== undefined) {
                if(params.data.r9.indexOf('-') == -1 ){
                  if(params.data.r9.indexOf('(0)') == -1 ){
                    return {color: '#5aff16'};
                  } else return {color: '#fff'}
                }
                else return {
                  color: '#ff6767'
                };
                }
              },
            // "suppressResize":true,
          },
          {
            "headerName":"R-10", 
            "field":"r10",
            cellStyle: function (params) {
              if (params.data !== undefined) {
                if(params.data.r10.indexOf('-') == -1 ){
                  if(params.data.r10.indexOf('(0)') == -1 ){
                    return {color: '#5aff16'};
                  } else return {color: '#fff'}
                }
                else return {
                  color: '#ff6767'
                };
                }
              },
            // "suppressResize":true,
          },
          {
            "headerName":"R-11", 
            "field":"r11",
            cellStyle: function (params) {
              if (params.data !== undefined) {
                if(params.data.r11.indexOf('-') == -1 ){
                  if(params.data.r11.indexOf('(0)') == -1 ){
                    return {color: '#5aff16'};
                  } else return {color: '#fff'}
                }
                else return {
                  color: '#ff6767'
                };
                }
              },
            // "suppressResize":true,
          },
      ]
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // 
    // params.api.sizeColumnsToFit();
    var allColumnIds = [];
    this.gridColumnApi.getAllColumns().forEach(function(column) {
      allColumnIds.push(column.colId);
    });
    this.gridColumnApi.autoSizeColumns(allColumnIds);
    // 
    
  
    this.http
      // SERVER BENER
      .get(
          // "http://10.54.36.49/api-license/public/api/egbts/layer2?vendor="+this.vendors+"&token="+this.token
          "http://10.54.36.49/api-license/public/api/egbts/layer2?vendor="+this.vendors+"&type="+types+"&token="+this.token
        )
        
      .subscribe(datas => {
        this.rowData = datas;
      });
      
      

      setTimeout(function() {
        var rowCount = 0;
        params.api.forEachNode(function(node) {
          node.setExpanded(rowCount++ === 1);
        });
      }, 500);
    }
    

    ngOnInit() {
      // console.log("data", this.data)
      // console.log("tipe", types)
      this.vendors = this.data.dataRes[0].vendor
      // this.tipes = types
      // this.types = this.gridApi.getFocusedCell().column.colId
      // console.log("types", this.types)
      // console.log("vendors", this.vendors)
      this.token = sessionStorage.getItem("token")  
    }

  closeDialog(){
      this.dialogRef.close();
  }
  
}

