import {Component, Input} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Store} from '@ngrx/store';
import {GridOptions} from 'ag-grid';
import { AgHeaders } from './common.model';


@Component({
  selector: 'ag-license-vendor-l2',
  template: `       
  <ag-grid-angular
  #agGrid
  style="width: 100%; height: 200px;"
  id="myGrid"
  [rowData]="data"
  class="ag-theme-dark"
  [columnDefs]="colDefs"
  [animateRows]="true"
  [enableSorting]="true"
  [enableFilter]="true"
  [enableColResize]="true"  
  (gridReady)="onGridReady($event)"
  (firstDataRendered)="onFirstDataRendered($event)"  
  ></ag-grid-angular>
  `,
  // 
  styles: [`
    
  `]
})
export class AgLicenseVendorL2 {
  private gridApi;
  private gridColumnApi;
  private rowData: any;
  @Input() colDefs:any;
  @Input() data:any;


  constructor(private store: Store<any>, public dialog: MatDialog) {
    // this.rowData = this.data
    // console.log("data l2",this.data)
    // console.log("colDefs",this.colDefs)
  }

  onFirstDataRendered(params) {
    params.api.sizeColumnsToFit();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;  
  }
  
}
