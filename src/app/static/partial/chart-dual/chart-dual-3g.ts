import {Component, OnInit, AfterViewInit, Input} from "@angular/core";
import { AmChartsService } from "@amcharts/amcharts3-angular";

@Component({
    selector: 'chart-dual-vendor-3g',
    template: `  
    <div style="color: white;margin-left: 10px;">{{vendor}}</div>  
   <!-- <div class="chartdiv" [id]="id"></div> -->
    <div style="width:100%;height:130px;margin-top:-15px;" [id]="id"></div>
    `,
    styles: [`
    .chartdiv {
      width		: 500px;
      height    : 130px;
      font-size	: 11px;
      margin-top  : -15px;
    }
    `]
  })
//  
export class ChartDualVendor3G implements AfterViewInit {
    @Input() id;
    @Input() vendor;
    @Input() data;

    constructor(private amchartService: AmChartsService){
  
    }
    ngAfterViewInit(){
        // console.log("id", this.id)
        // console.log("data", this.data)
        this.createChart(this.id, this.data)
    }

    createChart(id, data){
      let say = this.amchartService
      console.log("id",id)
      var chart = say.makeChart(id,
        {
          "type": "serial",
          "theme": "light",
          // "dataDateFormat": "YYYY-MM-DD",
          "precision": 2,

          "valueAxes": [{
            "id": "v1",
            "title": "",
            "position": "left",
            "autoGridCount": false,
            // "labelFunction": function(value) {
            //   return "$" + Math.round(value) + "M";
            // },
          "labelsEnabled": false
          }, {
            "id": "v2",
            "title": "",
            "gridAlpha": 0,
            "position": "right",
            "autoGridCount": false,
            "labelsEnabled": false,
          }],

          "graphs": [{
            // "id": "g3",
            "valueAxis": "v1",
            "lineColor": "#60a9ca",
            "fillColors": "#60a9ca",
            "fillAlphas": 1,
            "type": "column",
            // "title": "Actual Sales",
            // "valueField": "sales2",
            "title": "Allocated",
            "valueField": "allocated1",
            "clustered": false,
            "columnWidth": 0.5,
            "legendValueText": "$[[value]]M",
            "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
          }, {
            // "id": "g4",
            "valueAxis": "v1",
            "lineColor": "#e8c304",
            "fillColors": "#e8c304",
            "fillAlphas": 1,
            "type": "column",
            // "title": "Target Sales",
            // "valueField": "sales1",   
            "title": "Used",
            "valueField": "used1",     
            "clustered": false,
            "columnWidth": 0.3,
            "legendValueText": "$[[value]]M",
            "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
          }, {
            // "id": "g1",
            "valueAxis": "v2",
            // "title": "Market Days",
            // "valueField": "market1",
            "title": "Allocated",
            "valueField": "allocated2",
            "lineColor": "#60a9ca",
            "fillColors": "#60a9ca",
            "fillAlphas": 1,
            "type": "column",
            "clustered": false,
            "columnWidth": 0.5,
            "legendValueText": "[[value]]",
            "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
          }, 
          {
            // "id": "g2",
            "valueAxis": "v2",
            // "title": "Market Days ALL",
            // "valueField": "market2",
            "title": "Used",
            "valueField": "used2",
            "lineColor": "#e8c304",
            "fillColors": "#e8c304",
            "fillAlphas": 1,
            "type": "column",
            "clustered": false,
            "columnWidth": 0.3,
            "legendValueText": "[[value]]",
            "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]</b>"
          }],

          


          // "chartScrollbar": {
          //   "graph": "g1",
          //   "oppositeAxis": false,
          //   "offset": 30,
          //   "scrollbarHeight": 50,
          //   "backgroundAlpha": 0,
          //   "selectedBackgroundAlpha": 0.1,
          //   "selectedBackgroundColor": "#888888",
          //   "graphFillAlpha": 0,
          //   "graphLineAlpha": 0.5,
          //   "selectedGraphFillAlpha": 0,
          //   "selectedGraphLineAlpha": 1,
          //   "autoGridCount": true,
          //   "color": "#AAAAAA"
          // },

          "chartCursor": {
            "pan": true,
            "valueLineEnabled": true,
            // "valueLineBalloonEnabled": true,
            "cursorAlpha": 0,
            "valueLineAlpha": 0.2
          },
          // "categoryField": "date",
          "categoryField": "license",
          "categoryAxis": {
            "gridPosition": "start",
            // "parseDates": true,
            "dashLength": 1,
            "minorGridEnabled": true,
            // "labelRotation": 45,
            "fontSize": 0,
            
          },
          // "legend": {
          //   "useGraphSettings": true,
          //   "position": "top"
          // },
          "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0
          },
          // "export": {
          //  "enabled": true
          // },
  
          "dataProvider": data,
  
        //   "dataProvider": [
        //     {
        //     "license": "Cell FDD",
        //     // "market1": 71,
        //     // "market2": 75,
        //     // "sales1": 5,
        //     // "sales2": 8,
        //     "used1": 5,
        //     "allocated1": 8,
        //   }, {
        //     "license": "Cell TDD",
        //     "used1": 4,
        //     "allocated1": 6
        //   }, {
        //     "license": "FDD Bandwidth (per 5MHz)",
        //     "used1": 5,
        //     "allocated1": 2
        //   }, {
        //     "license": "TDD Bandwidth (per 5MHz)",
        //     "used1": 8,
        //     "allocated1": 9
        //   }, {
        //     "license": "FDD Power (Watt)",
        //     "used2": 82,
        //     "allocated2": 89,

        //   }, {
        //     "license": "TDD Power (Watt)",
        //     // "market1": 83,
        //     // "market2": 85,
        //     // "sales1": 3,
        //     // "sales2": 5,
        //     "used2": 82,
        //     "allocated2": 89,
        //   }, 
        // ]
        });
    }
}






