import { Component, AfterViewInit, ViewChild,Input, OnInit } from '@angular/core';
import 'ag-grid-enterprise';
import {AmChart, AmChartsService} from '@amcharts/amcharts3-angular';
import { Store } from '@ngrx/store';
import { selectorSettings } from '../../../settings/settings.reducer';

@Component({
  selector: 'amchart-bts2',
  templateUrl: './amchart-bts2.html',
  styleUrls: ['./amchart-bts2.css']
})

export class AmchartBts2Component implements AfterViewInit, OnInit {
  @Input() id:any;
  chart: any;
    constructor(private AmCharts: AmChartsService, private store: Store<any>) {
        
    }
  
    ngOnInit(){
        
    }

    ngAfterViewInit() {
        // this.store.select(selectorSettings).subscribe(data => {
        //     let datas = data.btsChart            
        //     if(datas != undefined){
        //         console.log("bts chart",datas)
        //         this.createChart(this.id)
        //     }            
        // })
        this.createChart(this.id)
    }
  
    createChart(id){
        let self = this
        var say = this.AmCharts        
        var chart = say.makeChart(id, {
            "type": "serial",
            "theme": "dark",
            // "responsive": {
            //     "enabled": true
            // },
            // "marginRight": 40,
            "marginLeft": 0,
            "dataProvider": [{
                "lineColor": "#ee8b6a",
                "date": "2012-01-01",
                "duration": 408
            }, {
                "date": "2012-01-02",
                "duration": 482
            }, {
                "date": "2012-01-03",
                "duration": 562
            }, {
                "date": "2012-01-08",
                "duration": 309,
                "lineColor": "#adf22e"
            }, {
                "date": "2012-01-09",
                "duration": 287
            }, {
                "date": "2012-01-10",
                "duration": 485
            }],
            "balloon": {
                "cornerRadius": 6,
                "horizontalPadding": 15,
                "verticalPadding": 10
            },
            "valueAxes": [{
                "duration": "mm",
                "durationUnits": {
                    "hh": "h ",
                    "mm": "min"
                },
                "axisAlpha": 0,
                "labelsEnabled": false
            }],
            "graphs": [{
                "bullet": "square",
                "bulletBorderAlpha": 1,
                "bulletBorderThickness": 1,
                "fillAlphas": 0.3,
                "fillColorsField": "lineColor",
                "legendValueText": "[[value]]",
                "lineColorField": "lineColor",
                "title": "duration",
                "valueField": "duration"
            }],
            "chartScrollbar": {
        
            },
            "chartCursor": {
                "categoryBalloonDateFormat": "YYYY MMM DD",
                "cursorAlpha": 0,
                "fullWidth": true
            },
            "dataDateFormat": "YYYY-MM-DD",
            "categoryField": "date",
            "categoryAxis": {
                "dateFormats": [{
                    "period": "DD",
                    "format": "DD"
                }, {
                    "period": "WW",
                    "format": "MMM DD"
                }, {
                    "period": "MM",
                    "format": "MMM"
                }, {
                    "period": "YYYY",
                    "format": "YYYY"
                }],
                "parseDates": true,
                "autoGridCount": false,
                "axisColor": "#555555",
                "gridAlpha": 0,
                "gridCount": 50,
                "labelsEnabled": false
            },
            "export": {
                "enabled": false
            }
            // ,
            // "listeners": [{
            //     "event": "dataUpdated",
            //     "method": function handleZoom(event) {
            //         event.zoomToDates(new Date(2012, 0, 3), new Date(2012, 0, 11));
            //      }
            // }]
        });
        
        
        
        chart.addListener("rendered", zoomChart);
        
        function zoomChart() {
            chart.zoomToDates(new Date(2012, 0, 3), new Date(2012, 0, 11));
        }
    }

    ngOnDestroy() {
        // this.AmCharts.destroyChart(chart);
      }
  
  }