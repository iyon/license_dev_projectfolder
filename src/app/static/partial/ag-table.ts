  import { Component, OnInit, ViewChild, Input  } from '@angular/core';
  import { HttpClient } from "@angular/common/http";
  import { environment as env } from '@env/environment';
  import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core';
  import 'ag-grid-enterprise';
  import {FeatureService} from '../provider/feature-service';

  @   Component({
    selector: 'ag-table',
    template: `
    <!--<ag-grid-angular
      #agGrid
      style="width: 100%; height: 100%;"
      id="myGrid"
      [rowData]="data"
      class="ag-theme-balham"
      [columnDefs]="colDefs"
      [enableColResize]="true"
      (firstDataRendered)="onFirstDataRendered($event)"
      ></ag-grid-angular>-->


      <ag-grid-angular
      #agGrid
      class="ag-theme-dark"
      style="width: 100%;height: 100%;"
      [rowData]="data"
      [columnDefs]="colDefs"    
      [enableColResize]="true"
      (firstDataRendered)="onFirstDataRendered($event)"

      [animateRows]="true"
      [groupDefaultExpanded]="groupDefaultExpanded"
      
    >  
      
    </ag-grid-angular>
    <!--[pagination] = "true" [enableSorting]="true"                      
    [enableFilter]="true"    
    [rowDeselection]="true"
    
      [enableSorting]="true"
      [enableFilter]="true"
      [rowSelection]="rowSelection"
      [groupMultiAutoColumn]="true"
      (gridReady)="onGridReady($event)"-->
    `,

    // styleUrls: ['./license.component.scss']
  })
  
  export class AgTable {
    @Input() data: any[];
    @Input() colDefs: any ;
    private gridApi;
    private gridColumnApi;
    autoGroupColumnDef;
    rowSelection: any;
    groupDefaultExpanded;

    constructor(private http: HttpClient) {
      this.groupDefaultExpanded = -1;
      // this.rowSelection = "single";
    }

    onFirstDataRendered(params) {
      params.api.sizeColumnsToFit();
    }

    onGridReady(params) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;

    }

  }