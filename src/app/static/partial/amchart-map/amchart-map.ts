import { Component, AfterViewInit, ViewChild,Input } from '@angular/core';
import 'ag-grid-enterprise';
import {AmChart, AmChartsService} from '@amcharts/amcharts3-angular';
import { Store } from '@ngrx/store';
import { selectorSettings } from '../../../settings/settings.reducer';

@Component({
  selector: 'amchart-map',
  templateUrl: './amchart-map.html',
  styleUrls: ['./amchart-map.css']
})

export class AmchartMapComponent implements AfterViewInit {
  @Input() id : any;

  constructor(private amchartService: AmChartsService, private store: Store<any>) {
    
  }

  ngAfterViewInit() {
    // var d = [{"field":"local_cell","value":"0"},{"field":"pa_xxdbm","value":"0"},{"field":"umpt_multi_mode_license_umts_per_umpt","value":"0"},{"field":"multi_mode_multi_band_license_for_multi_band_rf","value":"0"},{"field":"multimode_license_for_5000_series_rf_module_umts_per_ru","value":"0"},{"field":"multi_carrier_60w_high_power_license_per_trx","value":"0"},{"field":"umpt_multi_mode_license_gsm_per_umpt","value":"0"},{"field":"multi_mode_multi_band_license_for_multi_band_per_band_per_rru","value":"0"},{"field":"multimode_license_for_5000_series_rf_module_gsm_per_ru","value":"0"}]
    this.store.select(selectorSettings).subscribe(datas => {
      // console.log("pieDatas",datas.pieData)
      this.createMap(datas.pieData)
    })
  }

  createMap(data: any){
    var say = this.amchartService;
    var chart = say.makeChart(this.id, {
      "type": "map",
      "theme": "light",
      "projection": "mercator",         
      "dataProvider": {
        "map": "indonesiaHigh",    
        'zoomLevel': 1.1,
        // 'zoomLatitude': -8.403393,
        // 'zoomLongitude': 120.815173,    
        'areas': [
          /*Sumbagut*/
          {
            'title': 'Sumatera Utara',
            'id': 'ID-SU',
            'color': '#32ff19',
            'label': 'Stockholm: +8C',
            'customData': 'satu',
            'groupId': 'satuId'
          },
          {
            'title': 'Aceh',
            'id': 'ID-AC',
            'color': '#32ff19',
            'label': 'Stockholm: +8C',
            'customData': 'satu',
            'groupId': 'satuId'
          },


          /*Sumbagteng*/
          {
            'title': 'Sumatera Barat',
            'id': 'ID-SB',
            'color': '#a2d015',
            'label': 'Stockholm: +8C',
            'customData': 'dua',
            'groupId': 'duaId'
          },
          {
            'title': 'Kepulauan Riau',
            'id': 'ID-KR',
            'color': '#a2d015',
            'label': 'Stockholm: +8C',
            'customData': 'dua',
            'groupId': 'duaId'
          }, {
            'title': 'Riau',
            'id': 'ID-RI',
            'color': '#a2d015',
            'label': 'Stockholm: +8C',
            'customData': 'dua',
            'groupId': 'duaId'
          },

          /*Sumbagsel*/
          {
            'title': 'Sumatera Selatan',
            'id': 'ID-SS',
            'color': '#14bb00',
            'customData': 'tiga',
            'groupId': 'tigaId'
          }, {
            'title': 'Bengkulu',
            'id': 'ID-BE',
            'color': '#14bb00',
            'customData': 'tiga',
            'groupId': 'tigaId'
          }, {
            'title': 'Lampung',
            'id': 'ID-LA',
            'color': '#14bb00',
            'customData': 'tiga',
            'groupId': 'tigaId'
          }, {
            'title': 'Bangka Belitung',
            'id': 'ID-BB',
            'color': '#14bb00',
            'customData': 'tiga',
            'groupId': 'tigaId'
          }, {
            'title': 'Jambi',
            'id': 'ID-JA',
            'color': '#14bb00',
            'customData': 'tiga',
            'groupId': 'tigaId'
          },

          /*Jabodetabek*/
          {
            'title': 'Jakarta Raya',
            'id': 'ID-JK',
            'color': '#ffd200',
            'customData': 'empat',
            'groupId': 'empatId'
          }, {
            'title': 'Banten',
            'id': 'ID-BT',
            'color': '#ffd200',
            'customData': 'empat',
            'groupId': 'empatId'
          },

          /*Jabar*/
          {
            'title': 'Jawa Barat',
            'id': 'ID-JB',
            'color': '#e7ff21cf',
            'customData': 'lima',
            'groupId': 'limaId'
          },

          /*Jateng*/
          {
            'title': 'Jawa Tengah',
            'id': 'ID-JT',
            'color': '#43ff97',
            'customData': 'enam',
            'groupId': 'enamId'
          }, {
            'title': 'Yogyakarta',
            'id': 'ID-YO',
            'color': '#43ff97',
            'customData': 'enam',
            'groupId': 'enamId'
          },

          /*Jatim*/
          {
            'title': 'Jawa Timur',
            'id': 'ID-JI',
            'color': '#8bf371',
            'customData': 'tujuh',
            'groupId': 'tujuhId'
          },

          /*Kalimantan*/
          {
            'title': 'Kalimantan Tengah',
            'id': 'ID-KT',
            'color': '#8BC34A',
            'label': 'Stockholm: +8C',
            'customData': 'delapan',
            'groupId': 'delapanId'
          },
          {
            'title': 'Kalimantan Utara',
            'id': 'ID-KU',
            'color': '#8BC34A',
            'customData': 'delapan',
            'groupId': 'delapanId'
          },
          {
            'title': 'Kalimantan Barat',
            'id': 'ID-KB',
            'color': '#8BC34A',
            'customData': 'delapan',
            'groupId': 'delapanId'
          }, {
            'title': 'Kalimantan Timur',
            'id': 'ID-KI',
            'color': '#8BC34A',
            'customData': 'delapan',
            'groupId': 'delapanId'
          }, {
            'title': 'Kalimantan Selatan',
            'id': 'ID-KS',
            'color': '#8BC34A',
            'customData': 'delapan',
            'groupId': 'delapanId'
          },

          /*Sulawesi*/
          {
            'title': 'Sulawesi Utara',
            'id': 'ID-SA',
            'color': '#cfdc2bd9',
            'label': 'Stockholm: +8C',
            'customData': 'sembilan',
            'groupId': 'sembilanId'
          },
          {
            'title': 'Sulawesi Tenggara',
            'id': 'ID-SG',
            'color': '#cfdc2bd9',
            'customData': 'sembilan',
            'groupId': 'sembilanId'
          },
          {
            'title': 'Sulawesi Selatan',
            'id': 'ID-SN',
            'color': '#cfdc2bd9',
            'customData': 'sembilan',
            'groupId': 'sembilanId'
          }, {
            'title': 'Sulawesi Barat',
            'id': 'ID-SR',
            'color': '#cfdc2bd9',
            'customData': 'sembilan',
            'groupId': 'sembilanId'
          }, {
            'title': 'Sulawesi Tengah',
            'id': 'ID-ST',
            'color': '#cfdc2bd9',
            'customData': 'sembilan',
            'groupId': 'sembilanId'
          }, {
            'title': 'Gorontalo',
            'id': 'ID-GO',
            'color': '#cfdc2bd9',
            'customData': 'sembilan',
            'groupId': 'sembilanId'
          },
          {
            'title': 'Maluku Utara',
            'id': 'ID-MU',
            'color': '#cfdc2bd9',
            'customData': 'sembilan',
            'groupId': 'sembilanId'
          },

          /*Balinusra*/
          {
            'title': 'Bali',
            'id': 'ID-BA',
            'color': '#edff00',
            'customData': 'sepuluh',
            'groupId': 'sepuluhId'
          }, {
            'title': 'Nusa Tenggara Barat',
            'id': 'ID-NB',
            'color': '#edff00',
            'customData': 'sepuluh',
            'groupId': 'sepuluhId'
          }, {
            'title': 'Nusa Tenggara Timur',
            'id': 'ID-NT',
            'color': '#edff00',
            'customData': 'sepuluh',
            'groupId': 'sepuluhId'
          },

          /*Puma*/
          {
            'title': 'Maluku',
            'id': 'ID-MA',
            'color': '#91ff12',
            'customData': 'sebelas',
            'groupId': 'sebelasId'
          },
          {
            'title': 'Papua',
            'id': 'ID-PA',
            'color': '#91ff12',
            'customData': 'sebelas',
            'groupId': 'sebelasId'
          }, {
            'title': 'Papua Barat',
            'id': 'ID-PB',
            'color': '#91ff12',
            'customData': 'sebelas',
            'groupId': 'sebelasId'
        }],        
        "images": [          
          {
          "id": "sumbagut",
          'title': 'REGIONAL 1',          
          'latitude': 3.36366,
          'longitude': 98.0558802,
          "width": 50,
          "height": 50,
          "pie": {
            // "showBalloon": false,
            "type": "pie",
            "pullOutRadius": 0,
            "labelRadius": 0,
            "dataProvider": data,
            "labelText": "",
            "valueField": "value",
            "titleField": "field",
            "balloon":{
              "enabled":false,   
              "fixedPosition":true
             }
          }
        },
        {
          "id": "sumbagsel",
          'title': 'REGIONAL 2',
          'latitude': -2.9880025,
          'longitude': 103.6698743,
          "width": 50,
          "height": 50,
          "pie": {
            // "showBalloon": false,
            "type": "pie",
            "pullOutRadius": 0,
            "labelRadius": 0,
            "dataProvider": data,
            "labelText": "",
            "valueField": "value",
            "titleField": "field",
            "balloon":{
              "enabled":false,   
              "fixedPosition":true
             }
          }
        },
        {
          "id": "jabodetabek",
          'title': 'REGIONAL 3',
          'latitude': -6.280889,
          'longitude': 105.836160,
          "width": 50,
          "height": 50,
          "pie": {
            // "showBalloon": false,
            "type": "pie",
            "pullOutRadius": 0,
            "labelRadius": 0,
            "dataProvider": data,
            "labelText": "",
            "valueField": "value",
            "titleField": "field",
            "balloon":{
              "enabled":false,   
              "fixedPosition":true
             }
          }
        },
        {
          "id": "jawa-barat",
          'title': 'REGIONAL 4',
          'latitude': -7.690294,
          'longitude': 108.107584,
          "width": 50,
          "height": 50,
          "pie": {
            // "showBalloon": false,
            "type": "pie",
            "pullOutRadius": 0,
            "labelRadius": 0,
            "dataProvider": data,
            "labelText": "",
            "valueField": "value",
            "titleField": "field",
            "balloon":{
              "enabled":false,   
              "fixedPosition":true
             }
          }
        },
        {
          "id": "jawa-tengah",
          'title': 'REGIONAL 5',
          'latitude': -6.536396,
          'longitude': 110.963204,
          "width": 50,
          "height": 50,
          "pie": {
            // "showBalloon": false,
            "type": "pie",
            "pullOutRadius": 0,
            "labelRadius": 0,
            "dataProvider": data,
            "labelText": "",
            "valueField": "value",
            "titleField": "field",
            "balloon":{
              "enabled":false,   
              "fixedPosition":true
             }
          }
        },
        {
          "id": "jawa-timur",
          'title': 'REGIONAL 6',
          'latitude': -7.999180,
          'longitude': 114.129319,
          "width": 50,
          "height": 50,
          "pie": {
            // "showBalloon": false,
            "type": "pie",
            "pullOutRadius": 0,
            "labelRadius": 0,
            "dataProvider": data,
            "labelText": "",
            "valueField": "value",
            "titleField": "field",
            "balloon":{
              "enabled":false,   
              "fixedPosition":true
             }
          }
        },
        {
          "id": "balinusra",
          'title': 'REGIONAL 7',
          'latitude': -8.509375,
          'longitude': 120.842449,
          "width": 50,
          "height": 50,
          "pie": {
            // "showBalloon": false,
            "type": "pie",
            "pullOutRadius": 0,
            "labelRadius": 0,
            "dataProvider": data,
            "labelText": "",
            "valueField": "value",
            "titleField": "field",
            "balloon":{
              "enabled":false,   
              "fixedPosition":true
             }
          }
        },
        {
          "id": "kalimantan",
          'title': 'REGIONAL 8',
          'latitude': -0.5659583,
          'longitude': 114.3199371,
          "width": 50,
          "height": 50,
          "pie": {
            // "showBalloon": false,
            "type": "pie",
            "pullOutRadius": 0,
            "labelRadius": 0,
            "dataProvider": data,
            "labelText": "",
            "valueField": "value",
            "titleField": "field",
            "balloon":{
              "enabled":false,   
              "fixedPosition":true
             }
          }
        },
        {
          "id": "sulawesi",
          'title': 'REGIONAL 9',
          'latitude': -2.094776,
          'longitude': 120.447073,
          "width": 50,
          "height": 50,
          "pie": {
            // "showBalloon": false,
            "type": "pie",
            "pullOutRadius": 0,
            "labelRadius": 0,
            "dataProvider": data,
            "labelText": "",
            "valueField": "value",
            "titleField": "field",
            "balloon":{
              "enabled":false,   
              "fixedPosition":true
             }
          }
        },
        {
          "id": "sumbagteng",
          'title': 'REGIONAL 10',
          'latitude': 0.239975,
          'longitude': 101.086164,
          "width": 50,
          "height": 50,
          "pie": {
            // "showBalloon": false,
            "type": "pie",
            "pullOutRadius": 0,
            "labelRadius": 0,
            "dataProvider": data,
            "labelText": "",
            "valueField": "value",
            "titleField": "field",
            "balloon":{
              "enabled":false,   
              "fixedPosition":true
             }
          }
        },
        {
          "id": "puma",
          'title': 'REGIONAL 11',
          'latitude': -3.822807,
          'longitude': 136.584241,
          "width": 50,
          "height": 50,
          "pie": {
            // "showBalloon": true,
            // "hideBalloonTime":1000,
            "type": "pie",
            "pullOutRadius": 0,
            "labelRadius": 0,
            "dataProvider": data,
            "labelText": "",
            "valueField": "value",
            "titleField": "field",
            "balloon":{         
              "enabled":false,     
              "fixedPosition":true
             }
          }
        }]
      },
      
      /**
       * Add event to execute when the map is zoomed/moved
       * It also is executed when the map first loads
       */
      "listeners": [{
        "event": "positionChanged",
        "method": updateCustomMarkers
      }]
    });
    

    function legendeh(event){
      var legend = event.chart.legend
      legend = {
        "backgroundColor": "#fff",
        "backgroundAlpha": 0.7,
        "width": 200,
        "maxColumns": 1,
        // "horizontalGap": 30,
        // "position": "right",
        "right": 0,
        "align": "center",
        "data": [{
          "title": "One #3366CC ",
          "color": "#3366CC"
        }, {
          "title": "Two #3366CC ",
          "color": "#FFCC33"
        }]
      };
      // console.log("masuk")
    }
    /**
     * Creates and positions custom markers (pie charts)
     */
    function updateCustomMarkers(event) {      
      // get map object
      var map = event.chart;
 
    
      // go through all of the images
      for (var x = 0; x < map.dataProvider.images.length; x++) {
    
        // get MapImage object
        var image = map.dataProvider.images[x];
    
        // Is it a Pie?
        if (image.pie === undefined) {
          continue;
        }
    
        // create id
        if (image.id === undefined) {
          image.id = "amcharts_pie_" + x;
        }
        // Add theme
        if ("undefined" == typeof image.pie.theme) {
          image.pie.theme = map.theme;
        }
    
        // check if it has corresponding HTML element
        if ("undefined" == typeof image.externalElement) {
          image.externalElement = createCustomMarker(image);
        }
    
        // reposition the element accoridng to coordinates
        var xy = map.coordinatesToStageXY(image.longitude, image.latitude);
        image.externalElement.style.top = xy.y + "px";
        image.externalElement.style.left = xy.x + "px";
        image.externalElement.style.marginTop = Math.round(image.height / -2) + "px";
        image.externalElement.style.marginLeft = Math.round(image.width / -2) + "px";
      }
    }
    
    var ids = Array();
    /**
     * Creates a custom map marker - a div for container and a
     * pie chart in it
     */
    function createCustomMarker(image) {
      
      // Create chart container
      var holder = document.createElement("div");
      holder.id = image.id;
      // holder.title = image.title;
      holder.title = image.title;
      holder.style.position = "absolute";
      holder.style.width = image.width + "px";
      holder.style.height = image.height + "px";
    
      // Append the chart container to the map container
      image.chart.chartDiv.appendChild(holder);
    
      // Create a pie chart
      var chart = say.makeChart(image.id, image.pie);

      
    
      return holder;
    }
    
  }
}