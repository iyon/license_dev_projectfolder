import {Component, Input} from "@angular/core";
import { tembusan, listPerson, tembusan2 } from '../../data/data-pdf';
import { FeatureService } from "../../provider/feature-service";

@Component({
  selector: 'nodin',
  templateUrl: './nodin.html',
  styleUrls: ['./nodin.css']  
})
export class Nodin {   
    tablePdf1:any;
    tablePdf2:any;
    tablePdf3:any;
    tembusan:any;
    tembusan2:any;
    listPerson:any;

    constructor(private featureService: FeatureService) {        
        this.featureService.NodinTable1().subscribe(data => {            
            this.tablePdf1 = data            
        })
        this.featureService.NodinTable2().subscribe(data => {            
            this.tablePdf2 = data            
        })
        this.featureService.NodinTable3().subscribe(data => {            
            this.tablePdf3 = data            
        })        
        this.tembusan = tembusan;
        this.tembusan2 = tembusan2;
        this.listPerson = listPerson;
    }

    printElem(divId) {
        var content = document.getElementById(divId).innerHTML;
        var mywindow = window.open('', 'Print', 'height=600,width=800');
    
        mywindow.document.write('<html><head><title>Print</title>');
        mywindow.document.write('</head><body >');
        mywindow.document.write(content);
        mywindow.document.write('</body></html>');
    
        mywindow.document.close();
        mywindow.focus()
        // mywindow.print();
        // mywindow.close();
        return true;
    }
}