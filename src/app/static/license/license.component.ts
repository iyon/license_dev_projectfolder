import { Component, OnInit, ViewChild, AfterContentInit, AfterViewInit, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import 'ag-grid-enterprise';
import { AmChartsService, AmChart } from '@amcharts/amcharts3-angular';
import { FeatureService } from '../provider/feature-service';
import { egbtsColDefs, nodebColDefs, enodebColDefs, licenseChangeColDefs, 
  // tesTable
 } from '../model/ag-header.model';
import { Store } from '@ngrx/store';
import { ActionSettingsPieData, selectorSettings } from '../../settings/settings.reducer';
import { HttpClient } from '@angular/common/http';
import { Tes } from '../model/model';
import * as jspdf from 'jspdf';  
import html2canvas from 'html2canvas';  
import { dataTablePdf1 } from '../data/data-pdf';



@Component({
  selector: 'license',
  // templateUrl: './license.component.html',
  templateUrl: './license.component.v2.html',
  // templateUrl: './tes.html',
  styleUrls: ['./license.component.css']
  // encapsulation: ViewEncapsulation.None
})
export class LicenseComponent implements AfterViewInit, OnInit {
  //Map Chart
  pieData: any;
  id = "dadas"

  //License Chart
  licenseChart2g;
  licenseChart3g;
  licenseChart4g;
  newlicenseChart2g;
  powerlicenseChart2g;
  newlicenseChart3g;
  powerlicenseChart3g;
  newlicenseChart4g;
  powerlicenseChart4g;
  bandlicenseChart4g;

  //License Table
  egbtsData : any;
  nodebData : any;
  enodebData : any;
  egbtsColDefs : any;
  nodebColDefs : any;
  enodebColDefs : any;

  //License Change Table
  licenseChange2g : any;
  licenseChange3g : any;
  licenseChange4g : any;
  licenseChangeColDefs : any;
  licenseRegion2g;
  licenseRegion3g;
  licenseRegion4g;
  
  //Summary BTS On Air
  chartContainer2G = true;
  chartContainer3G = false;
  chartContainer4G = false;
  dtech: any;
  ddata: any;
  boaDummy: any;
  boaDummy2: any;
  dummyChart1: any;
  dummyChart2: any;
  dummyChart3: any;
  dummyDataChart2G;
  dummyDataChart3G;
  dummyDataChart4G;
  date;

  //Test
  objTes = [{"id":1, "name":"ayas"},{"id":2, "name":"swi"}]
  tesData: any = [{"id":0,"value":"asdf"},{"id":0,"value":"asdf"}];
  tesColDefs: any;
  testList:any[]= new Array();
  
  constructor(
    private amchartService: AmChartsService, 
    private featureService : FeatureService, 
    private store: Store<any>,
    private httpClient: HttpClient,
    private cdRef:ChangeDetectorRef
    ) {    

    /**
     * Map Chart
     */
    this.featureService.pieMap().subscribe(data => {
      this.store.dispatch(new ActionSettingsPieData({pieData: data}))
    })  



    /**
     * License Table
     */  
    // this.featureService.table2g().subscribe(data => {      
    //   this.egbtsData = data;
    // });
    this.featureService.Newtable2g().subscribe(data => {      
      this.egbtsData = data;
    });
    this.egbtsColDefs = egbtsColDefs;
    
    // this.featureService.table3g().subscribe(data => {      
    //   this.nodebData = data;
    // });

    this.featureService.Newtable3g().subscribe(data => {      
      this.nodebData = data;
    });
    this.nodebColDefs = nodebColDefs;
        
    // this.featureService.table4g().subscribe(data => {      
    //   this.enodebData = data;
    // });

    this.featureService.Newtable4g().subscribe(data => {      
      this.enodebData = data;
    });
    this.enodebColDefs = enodebColDefs;



    /**
     * License Change Table
     */    
    // this.featureService.licenseChange2g().subscribe(data => {      
    //   this.licenseChange2g = data;
    // });    
    
    this.featureService.licenseChange3g().subscribe(data => {      
      this.licenseChange3g = data;
    });

    this.featureService.licenseChange4g().subscribe(data => {      
      this.licenseChange4g = data;
    });


    this.featureService.LicenseRegion2g().subscribe(data => {      
      this.licenseRegion2g = data;
    });

    this.featureService.LicenseRegion3g().subscribe(data => {      
      this.licenseRegion3g = data;
    });

    this.featureService.LicenseRegion4g().subscribe(data => {      
      this.licenseRegion4g = data;
      // console.log("licenseRegion4g", this.licenseRegion4g)
    });

    this.licenseChangeColDefs = licenseChangeColDefs



    /**
     * BOA FORM
     */      
    this.featureService.boaFormTech().subscribe(data => {      
      this.dtech = data;      
    });

    this.featureService.boaFormData().subscribe(data => {      
      this.ddata = data;      
    });


    /** 
     * DUMMY DATA
     */

    this.licenseChange2g = [
      {
        "license": "TRX",
        "status": "Active",
        "r1": 10,
        "r2": 10,
        "r3": 10,
        "r4": 10,
        "r5": 10,
        "r6": 10,
        "r7": 10,
        "r8": 10,
        "r9": 10,
        "r10": 10,
        "r11": 10,
      },
      {
        "license": "TRX",
        "status": " Not Active",
        "r1": 0,
        "r2": 0,
        "r3": 10,
        "r4": 10,
        "r5": 10,
        "r6": 10,
        "r7": 10,
        "r8": 10,
        "r9": 10,
        "r10": 10,
        "r11": 10,
      },
      {
        "license": "TRX",
        "status": "Total",
        "r1": 10,
        "r2": 10,
        "r3": 20,
        "r4": 20,
        "r5": 20,
        "r6": 20,
        "r7": 20,
        "r8": 20,
        "r9": 20,
        "r10": 20,
        "r11": 20,
      },
      {
        "license": "Power",
        "status": "Active",
        "r1": 10,
        "r2": 10,
        "r3": 10,
        "r4": 10,
        "r5": 10,
        "r6": 10,
        "r7": 10,
        "r8": 10,
        "r9": 10,
        "r10": 10,
        "r11": 10,
      },
      {
        "license": "Power",
        "status": "Not Active",
        "r1": 0,
        "r2": 0,
        "r3": 0,
        "r4": 0,
        "r5": 0,
        "r6": 0,
        "r7": 0,
        "r8": 0,
        "r9": 0,
        "r10": 10,
        "r11": 10,
      },
      {
        "license": "Power",
        "status": "Total",
        "r1": 10,
        "r2": 10,
        "r3": 10,
        "r4": 10,
        "r5": 10,
        "r6": 10,
        "r7": 10,
        "r8": 10,
        "r9": 10,
        "r10": 20,
        "r11": 20,
      },
    ]
    this.boaDummy = [
      {"sinyal":"2G", "onair":10, "dismant": 5, "totski":15},
      {"sinyal":"3G", "onair":12, "dismant": 2, "totski":12},
      {"sinyal":"4G", "onair":5, "dismant": 7, "totski":12}
    ]

    this.boaDummy2 = [
      {"header":"TRX", "row1": "On Air: 10", "row2": "Dismantle: 9", "row3":"Total: 19"},
      {"header":"BSC/RNC", "row1": "BSC: 5", "row2": "RNC: 5", "row3":"Total: 10"},
      {"header":"Stand Alone", "row1": "2G Only: 4", "row2": "3G Only: 9", "row3":"4G Only: 13"},
    ]
    
    // this.dummyDataChart2G = [{
  //   this.egbtsData = [
  //   //   {
  //   //   "type": "Used",
  //   //   "values": 2025,
  //   //   "lineColor": "#ffffff"
  //   // },
  //   {
  //     "area": "AREA 1",
  //     "vendor": "HUAWEI",
  //     "trx_active": 315739,
  //     "trx_unactive": 14642,
  //     "trx_total": 330381,
  //     "power_active": 0,
  //     "power_unactive": 0,
  //     "power_total": 0,
  //   },
  //   {
  //     "area": "AREA 2",
  //     "vendor": "HUAWEI",
  //     "trx_active": 266882,
  //     "trx_unactive": 14697,
  //     "trx_total": 281579,
  //     "power_active": 0,
  //     "power_unactive": 0,
  //     "power_total": 0,
  //   },
  //   {
  //     "area": "AREA 3",
  //     "vendor": "HUAWEI",
  //     "trx_active": 220920,
  //     "trx_unactive": 28390,
  //     "trx_total": 249310,
  //     "power_active": 0,
  //     "power_unactive": 0,
  //     "power_total": 0,
  //   },
  // ]
  // console.log("egtbsdata", this.egbtsData)

    // this.dummyDataChart3G = [{
    // this.nodebData = [{
    //   "type": "Used",
    //   "values": 3025,
    //   "lineColor": "#ffffff"
    // },{
    //   "type": "Allo",
    //   "values": 3425,
    //   "lineColor": "#ffffff"
    // },{
    //   "type": "Used",
    //   "values": 3025,
    //   "lineColor": "#7cbeff"
    // },{
    //   "type": "Allo",
    //   "values": 3425,
    //   "lineColor": "#7cbeff"
    // }]
    
    // this.dummyDataChart4G = [{
    // this.enodebData = [{
    //   "type": "Used",
    //   "values": 4025,
    //   "lineColor": "#217a3c"
    //   },{
    //   "type": "Allo",
    //   "values": 5425,
    //   "lineColor": "#217a3c"
    //   },{
    //   "type": "Used",
    //   "values": 4025,
    //   "lineColor": "rgba(4, 99, 136, 0.945)"
    //   },{
    //   "type": "Allo",
    //   "values": 5425,
    //   "lineColor": "rgba(4, 99, 136, 0.945)"
    //   },{
    //   "type": "Used",
    //   "values": 4025,
    //   "lineColor": "#ffffff"
    //   },{
    //   "type": "Allo",
    //   "values": 5425,
    //   "lineColor": "#ffffff"
    //   },{
    //   "type": "Used",
    //   "values": 4025,
    //   "lineColor": "#434343"
    //   },{
    //   "type": "Allo",
    //   "values": 5425,
    //   "lineColor": "#434343"
    //   },{
    //   "type": "Used",
    //   "values": 4025,
    //   "lineColor": "#707070"
    //   },{
    //   "type": "Allo",
    //   "values": 5425,
    //   "lineColor": "#707070"
    //   },{
    //   "type": "Used",
    //   "values": 4025,
    //   "lineColor": "#ce1616"
    //   },{
    //   "type": "Allo",
    //   "values": 5425,
    //   "lineColor": "#ce1616"
    // }]
    
    this.dummyChart1 = [
      {"id": 1, "vendor": "HUAWEI", "data": this.dummyDataChart2G},
      {"id": 2, "vendor": "NOKIA", "data": this.dummyDataChart2G},
      {"id": 3, "vendor": "ERICSSON", "data": this.dummyDataChart2G},
      {"id": 4, "vendor": "ZTE", "data": this.dummyDataChart2G}
    ];
    this.dummyChart2 = [
      {"id": 5, "vendor": "HUAWEI", "data": this.dummyDataChart3G},
      {"id": 6, "vendor": "NOKIA", "data": this.dummyDataChart3G},
      {"id": 7, "vendor": "ERICSSON", "data": this.dummyDataChart3G},
      {"id": 8, "vendor": "ZTE", "data": this.dummyDataChart3G}
    ];
    this.dummyChart3 = [
      {"id": 9, "vendor": "HUAWEI", "data": this.dummyDataChart4G},
      {"id": 10, "vendor": "NOKIA", "data": this.dummyDataChart4G},
      {"id": 11, "vendor": "ERICSSON", "data": this.dummyDataChart4G},
      {"id": 12, "vendor": "ZTE", "data": this.dummyDataChart4G}
    ];

    

    /**
     * TEST
     */
    // var tes = [];
    // this.objTes.forEach(x => {
    //   tes.push(x.name)
    // })
    // console.log("tes",tes)

    //test table
    // this.testList.concat(this.tesData)
    // console.log(`teslis => ${JSON.stringify(this.testList)}`)
    // var a = []
    // var b = this.tesData
    // var d = 11
    // var dd = `{"id":${d},"value":"ccc"}`
    // var aa = dd
    // setInterval(() => {
    //   d += 1      
    //   aa += `,${dd}`
    //   console.log(`aa ${aa}`)
    //   // a = [dd]
    //   // var c = b.concat(a)
    //   this.tesData = [JSON.parse(aa)]
    //   // this.getTest()
    //   // console.log(`update/5min ${JSON.stringify(c)}`)
    //   console.log(`update/5min ${this.tesData}`)
    // },3000)
    // this.tesColDefs = tesTable;
        
  }

  
  dateStr(dates){
    // let listMonth = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    // let date = new Date(dates)
    // let monthIdx = date.getMonth()  
    // let year = date.getFullYear()
    // let monthStr = listMonth[monthIdx]
    // let year2Digit = year.toString().substring(2,4)
    // let result = `${monthStr}-${year2Digit}`
    // console.log(`cek date: ${result}`)
    // return result

    let date = new Date()
    let currentMIdx = date.getMonth()
    let monthIdx = currentMIdx - currentMIdx
    let year = date.getFullYear()
    let year2Digit = year.toString().substring(2,4)
    console.log(date)
  }

  ngAfterViewInit() {
    // this.dateStr("")
    // this.cdRef.detectChanges();

    /**
     * License Chart
     */
    // this.featureService.LicenseChart2G().subscribe(data => {
    //   this.licenseChart2g = data;
    //   // console.log(this.licenseChart2g)
    // });

    // this.featureService.LicenseChart3G().subscribe(data => {
    //   this.licenseChart3g = data;
    //   // console.log(this.licenseChart3g)
    // });

    // this.featureService.LicenseChart4G().subscribe(data => {
    //   this.licenseChart4g = data;
    //   // this.date = data.date
    //   // console.log(this.licenseChart4g)
    // });

    this.featureService.NewLicenseChart2G().subscribe(data => {
      // this.newlicenseChart2g = data;
      this.newlicenseChart2g = [data];
    });
    this.featureService.PowerLicenseChart2G().subscribe(data => {
      this.powerlicenseChart2g = [data];
      // this.date = data.date
      // console.log("PowerLicenseChart2G", this.powerlicenseChart2g)
    });


    this.featureService.NewLicenseChart3G().subscribe(data => {
      this.newlicenseChart3g = [data];
    });
    this.featureService.PowerLicenseChart3G().subscribe(data => {
      this.powerlicenseChart3g = [data];
      // console.log("PowerLicenseChart2G", this.powerlicenseChart2g)
    });


    this.featureService.NewLicenseChart4G().subscribe(data => {
      this.newlicenseChart4g = [data];
    });
    this.featureService.PowerLicenseChart4G().subscribe(data => {
      this.powerlicenseChart4g = [data];
      // console.log("PowerLicenseChart2G", this.powerlicenseChart2g)
    });
    this.featureService.BandLicenseChart4G().subscribe(data => {
      this.bandlicenseChart4g = [data];
      // console.log("bandlicenseChart4g", this.bandlicenseChart4g)
    });
    

  }

  ngOnInit(){

  }

  btn2G(evt) {
    var i, tablinks;    
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    this.chartContainer2G = true;
    this.chartContainer3G = false;
    this.chartContainer4G = false;
    evt.currentTarget.className += " active";
  }

  btn3G(evt) {
    var i, tablinks;    
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    this.chartContainer2G = false;
    this.chartContainer3G = true;
    this.chartContainer4G = false;
    evt.currentTarget.className += " active";
  }

  btn4G(evt) {
    var i, tablinks;    
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    this.chartContainer2G = false;
    this.chartContainer3G = false;
    this.chartContainer4G = true;
    evt.currentTarget.className += " active";
  }

  getTest(){        
    let str ="http://10.46.0.243/license/public/test";    
    this.httpClient.get<Tes>(str).subscribe(x =>{
      let result = {"id":x.id, "value": x.value}
      if(!this.testList.includes(result)){        
        this.testList.push(result);
        console.log(`current => ${JSON.stringify(result)}`);
        // console.log(`tes list length => ${JSON.stringify(this.testList)}`);
      }
      this.tesData = this.testList
      // this.testList = [result]   
      // console.log(`tes tis => ${JSON.stringify(this.tesData)}`)
      // console.log(`tes list length => ${JSON.stringify(this.testList)}`)
    })
  }

  
  
}
