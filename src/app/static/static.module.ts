import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared';

import { StaticRoutingModule } from './static-routing.module';
import { AboutComponent } from './about/about.component';
import { FeaturesComponent } from './features/features.component';
import {AgTable} from './partial/ag-table';
import {AgTableLicense} from './partial/ag-table-license';
import {AgGridModule} from 'ag-grid-angular';
import {FeatureService} from './provider/feature-service';
import { AgLicenseVendorL2 } from './partial/ag-license-vendor-l2';
import {LicenseComponent} from '@app/static/license/license.component';
import { AmchartMapComponent } from './partial/amchart-map/amchart-map';
import { AmChartsModule } from '@amcharts/amcharts3-angular';
import { ChartUpdownComponent } from './partial/chart-updown/chart-updown';
import { MatGridList, MatGridListModule } from '@angular/material';
import { ChartActivationComponent } from './partial/chart-activation/chart-activation';
import { BoaFormComponent } from './partial/boa-from/boa-form';
import { AmchartBtsComponent } from './partial/amchart-bts/amchart-bts';
import { LoginComponent } from './login/login';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AmchartBts2Component } from './partial/amchart-bts2/amchart-bts2';
import { BoaForm2Component } from './partial/boa-from2/boa-form2';
import { PdfBag1 } from './partial/pdf/pdf-bag1';
import { PdfBag2 } from './partial/pdf/pdf-bag2';
import { Nodin } from './mom-nodin/nodin/nodin';
import { Mom } from './mom-nodin/mom/mom';
import { HeadMom } from './partial/mom/head-mom/head-mom';
import { Paragraph } from './partial/mom/paragraph/paragraph';
import { TestComponent } from './test/testing.component';
import { ChartDynamic } from './test/amchart-dynamic/amchart-dynamic';
import { ChartHuawei } from './partial/chart-vendor/chart-huawei/chart-huawei';
import { ChartBarVendor } from './partial/chart-bar-vendor/chart-bar-vendor';
import { LicenseVendorLayer2 } from './partial/dialog/license-vendor-layer2/license-vendor-layer2';
import { ChartBarVendor4G } from './partial/chart-bar-vendor/chart-bar-vendor-4g';
import { LicenseVendorLayer3 } from './partial/dialog/license-vendor-layer2/license-vendor-layer3';
import { LicenseVendorLayer4 } from './partial/dialog/license-vendor-layer2/license-vendor-layer4';
// import { ChartDualVendor4G } from './partial/chart-dual/chart-dual';
// import { SquareComponent } from '../static/model/ag-header.model';
import { ChartDualVendor2G } from './partial/chart-dual/chart-dual-2g';
import { ChartDualVendor3G } from './partial/chart-dual/chart-dual-3g';
import { ChartDualVendor4G } from './partial/chart-dual/chart-dual-4g';
// import { ChartBarNew } from './partial/chart-bar-vendor/chart-bar-new';
import { ChartBarPower } from './partial/chart-bar-vendor/chart-bar-power';
import { ChartBarNew } from './partial/chart-bar-vendor/chart-bar-newTRX';
// import { ChartBarNewCell } from './partial/chart-bar-vendor/chart-bar-newCell';
import { ChartBarNewTest } from './partial/chart-bar-vendor/chart-bar-newTest';
import { ChartBarBand } from './partial/chart-bar-vendor/chart-bar-band';
import { ChartBarNewCell3G } from './partial/chart-bar-vendor/chart-bar-newCell';
import { ChartBarNewCell4G } from './partial/chart-bar-vendor/chart-bar-newCell4g';
import { ChartBarPower4G } from './partial/chart-bar-vendor/chart-bar-power4g';

@NgModule({
  imports: [
    SharedModule, 
    StaticRoutingModule, 
    AgGridModule,
    // AgGridTableComponent,
    AmChartsModule,
    MatGridListModule,
    AgGridModule.withComponents([AgLicenseVendorL2,AgTable,AgTableLicense]),
    MDBBootstrapModule.forRoot()
  ],
  declarations: [
    AboutComponent, 
    FeaturesComponent,
    LicenseComponent,
    AgTable,
    AgTableLicense,
    AgLicenseVendorL2,
    AmchartMapComponent,
    ChartUpdownComponent,
    ChartActivationComponent,
    BoaFormComponent,
    BoaForm2Component,
    AmchartBtsComponent,
    AmchartBts2Component,
    LoginComponent,
    PdfBag1,
    PdfBag2,
    Nodin,
    Mom,
    HeadMom,
    Paragraph,
    TestComponent,
    ChartDynamic,
    ChartHuawei,
    ChartBarVendor,
    ChartBarVendor4G,
    LicenseVendorLayer2,
    LicenseVendorLayer3,
    LicenseVendorLayer4,
    ChartDualVendor4G,
    ChartDualVendor2G,
    ChartDualVendor3G,  
    ChartBarNew, 
    ChartBarNewCell3G,
    ChartBarNewCell4G,
    ChartBarPower, 
    ChartBarPower4G,
    ChartBarNewTest,
    ChartBarBand,
    // SquareComponent    
  ],
  entryComponents: [LicenseVendorLayer2,LicenseVendorLayer3,LicenseVendorLayer4],
  providers: [
    FeatureService,
  ]
})
export class StaticModule {}
