// import { Component } from "@angular/core";
// import { AgRendererComponent } from "ag-grid-angular";

// @Component({
//   selector: 'square-cell',
//   template: `{{valueSquared()}}`
// })
// export class SquareComponent implements AgRendererComponent {
//   private params:any;

//   agInit(params:any):void {    
//       this.params = params;
//   }

//   private valueSquared():any {
//       return this.params.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
//   }  
//   refresh
// }

/*License*/
// *** TABLE 2G ***
export const egbtsColDefs = [
  // {headerName: 'Vendor', 
  // field: 'vendor',
  {headerName: 'AREA', 
  field: 'area', rowGroup: true, hide: true, width: 100,suppressSizeToFit: true,},

  // {headerName: 'Status', 
  // field: 'type', 
  {headerName: 'VENDOR', 
  field: 'vendor', 
  width: 100},

  // {headerName: 'TRX', 
  // field: 'trx',
  {headerName: 'JUMLAH TRX', 
  cellStyle: {
    "text-align": "center !important"
  },
  children: [
    {
      headerName: "ACTIVE",
      field: "trx_active",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['trx_active'].indexOf('-') == -1 ){
            if(params.data['trx_active'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },

    },
    {
      headerName: "NOT ACTIVE",
      field: "trx_unactive",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['trx_unactive'].indexOf('-') == -1 ){
            if(params.data['trx_unactive'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
      
    },
    {
      headerName: "TOTAL",
      field: "trx_total",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['trx_total'].indexOf('-') == -1 ){
            if(params.data['trx_total'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },

    },
  ],
  cellRenderer: valueToComma, width: 160, 
  },

  // {headerName: 'Power(Watt)', 
  // field: 'power', 
  {headerName: 'POWER(WATT)', 
  children: [
    {
      headerName: "ACTIVE",
      field: "power_active",

      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['power_active'].indexOf('-') == -1 ){
            if(params.data['power_active'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },

    },
    {
      headerName: "NOT ACTIVE",
      field: "power_unactive",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['power_unactive'].indexOf('-') == -1 ){
            if(params.data['power_unactive'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
    {
      headerName: "TOTAL",
      field: "power_total",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['power_total'].indexOf('-') == -1 ){
            if(params.data['power_total'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
  ], 
  cellRenderer: valueToComma, width: 160, 
  }
]
// *** TABLE 3G ***
export const nodebColDefs = [

  {headerName: 'AREA', 
  field: 'area', rowGroup: true, hide: true, width: 100,suppressSizeToFit: true,},


  {headerName: 'VENDOR', 
  field: 'vendor', 
  width: 100},


  {headerName: 'JUMLAH CELL CARRIER', 
  children: [
    {
      headerName: "ACTIVE",
      field: "trx_active",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['trx_active'].indexOf('-') == -1 ){
            if(params.data['trx_active'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
    {
      headerName: "NOT ACTIVE",
      field: "trx_unactive",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['trx_unactive'].indexOf('-') == -1 ){
            if(params.data['trx_unactive'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
    {
      headerName: "TOTAL",
      field: "trx_total",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['trx_total'].indexOf('-') == -1 ){
            if(params.data['trx_total'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
  ],
  cellRenderer: valueToComma, width: 160, 

  },


  {headerName: 'POWER(WATT)', 
  children: [
    {
      headerName: "ACTIVE",
      field: "power_active",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['power_active'].indexOf('-') == -1 ){
            if(params.data['power_active'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
    {
      headerName: "NOT ACTIVE",
      field: "power_unactive",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['power_unactive'].indexOf('-') == -1 ){
            if(params.data['power_unactive'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
    {
      headerName: "TOTAL",
      field: "power_total",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['power_total'].indexOf('-') == -1 ){
            if(params.data['power_total'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
  ], 
  cellRenderer: valueToComma, width: 160, 

  }
]

// *** TABLE 4G ***
export const enodebColDefs = [
  {headerName: 'AREA', 
  field: 'area', rowGroup: true, hide: true, width: 100,suppressSizeToFit: true,},


  {headerName: 'VENDOR', 
  field: 'vendor', 
  width: 100},


  {headerName: 'JUMLAH CELL CARRIER', 
  children: [
    {
      headerName: "ACTIVE",
      field: "trx_active",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['trx_active'].indexOf('-') == -1 ){
            if(params.data['trx_active'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
    {
      headerName: "NOT ACTIVE",
      field: "trx_unactive",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['trx_unactive'].indexOf('-') == -1 ){
            if(params.data['trx_unactive'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
    {
      headerName: "TOTAL",
      field: "trx_total",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['trx_total'].indexOf('-') == -1 ){
            if(params.data['trx_total'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
  ],
  cellRenderer: valueToComma, width: 160, 

  },


  {headerName: 'POWER(WATT)', 
  children: [
    {
      headerName: "ACTIVE",
      field: "power_active",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['power_active'].indexOf('-') == -1 ){
            if(params.data['power_active'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
    {
      headerName: "NOT ACTIVE",
      field: "power_unactive",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['power_unactive'].indexOf('-') == -1 ){
            if(params.data['power_unactive'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
    {
      headerName: "TOTAL",
      field: "power_total",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['power_total'].indexOf('-') == -1 ){
            if(params.data['power_total'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
  ], 
  cellRenderer: valueToComma, width: 160, 

  },

  {headerName: 'BANDWIDTH (Mhz)', 
  children: [
    {
      headerName: "ACTIVE",
      field: "bandwidth_active",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['bandwidth_active'].indexOf('-') == -1 ){
            if(params.data['bandwidth_active'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
    {
      headerName: "NOT ACTIVE",
      field: "bandwidth_unactive",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['bandwidth_unactive'].indexOf('-') == -1 ){
            if(params.data['bandwidth_unactive'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
    {
      headerName: "TOTAL",
      field: "bandwidth_total",
      cellStyle: function (params) {
        // console.log("params.data", params.data)
        if (params.data !== undefined) {
          // console.log ( "tes",params.data['trx'])
          if(params.data['bandwidth_total'].indexOf('-') == -1 ){
            if(params.data['bandwidth_total'].indexOf('(0)') == -1 ){
              return {color: 'rgba(48, 208, 220, 1)'};
            }else return {color: '#fff'}    
          }
          else return {
            color: 'rgba(252, 112, 115, 1)'
          };
        }
      },
    },
  ],
  cellRenderer: valueToComma, width: 160, 

  },
]


/* TABLE REGIONAL Change*/
export const licenseChangeColDefs = [
  {headerName: 'LICENSE', field: 'license', rowGroup: true, hide: true, width: 50,},
  {headerName: 'STATUS', field: 'status', width: 100},
  {headerName: 'R1', field: 'r1', width: 80, cellRenderer: valueToComma, 
  cellStyle: function (params) {
    // console.log("params.data", params.data)
   if (params.data !== undefined) {
     // console.log ( "tes",params.dataRes)
     if(params.data['r1'].indexOf('-') == -1 ){
       if(params.data['r1'].indexOf('(0)') == -1 ){
         return {color: 'rgba(48, 208, 220, 1)'};
       } else return {color: '#fff'}
     }
     else return {
       color: 'rgba(252, 112, 115, 1)'
     };
     }
   },

  },
  {headerName: 'R2', field: 'r2', width: 80, cellRenderer: valueToComma,
  cellStyle: function (params) {
    // console.log("params.data", params.data)
   if (params.data !== undefined) {
     // console.log ( "tes",params.dataRes)
     if(params.data['r2'].indexOf('-') == -1 ){
       if(params.data['r2'].indexOf('(0)') == -1 ){
         return {color: 'rgba(48, 208, 220, 1)'};
       } else return {color: '#fff'}
     }
     else return {
       color: 'rgba(252, 112, 115, 1)'
     };
     }
   },
  },
  {headerName: 'R3', field: 'r3', width: 80, cellRenderer: valueToComma, 
  cellStyle: function (params) {
    // console.log("params.data", params.data)
   if (params.data !== undefined) {
     // console.log ( "tes",params.dataRes)
     if(params.data['r3'].indexOf('-') == -1 ){
       if(params.data['r3'].indexOf('(0)') == -1 ){
         return {color: 'rgba(48, 208, 220, 1)'};
       } else return {color: '#fff'}
     }
     else return {
       color: 'rgba(252, 112, 115, 1)'
     };
     }
   },
  },
  {headerName: 'R4', field: 'r4', width: 80, cellRenderer: valueToComma,
  cellStyle: function (params) {
    // console.log("params.data", params.data)
   if (params.data !== undefined) {
     // console.log ( "tes",params.dataRes)
     if(params.data['r4'].indexOf('-') == -1 ){
       if(params.data['r4'].indexOf('(0)') == -1 ){
         return {color: 'rgba(48, 208, 220, 1)'};
       } else return {color: '#fff'}
     }
     else return {
       color: 'rgba(252, 112, 115, 1)'
     };
     }
   },
  },
  {headerName: 'R5', field: 'r5', width: 80, cellRenderer: valueToComma,
  cellStyle: function (params) {
    // console.log("params.data", params.data)
   if (params.data !== undefined) {
     // console.log ( "tes",params.dataRes)
     if(params.data['r5'].indexOf('-') == -1 ){
       if(params.data['r5'].indexOf('(0)') == -1 ){
         return {color: 'rgba(48, 208, 220, 1)'};
       } else return {color: '#fff'}
     }
     else return {
       color: 'rgba(252, 112, 115, 1)'
     };
     }
   },
  },
  {headerName: 'R6', field: 'r6', width: 80, cellRenderer: valueToComma,
  cellStyle: function (params) {
    // console.log("params.data", params.data)
   if (params.data !== undefined) {
     // console.log ( "tes",params.dataRes)
     if(params.data['r6'].indexOf('-') == -1 ){
       if(params.data['r6'].indexOf('(0)') == -1 ){
         return {color: 'rgba(48, 208, 220, 1)'};
       } else return {color: '#fff'}
     }
     else return {
       color: 'rgba(252, 112, 115, 1)'
     };
     }
   },
  },
  {headerName: 'R7', field: 'r7', width: 80, cellRenderer: valueToComma,
  cellStyle: function (params) {
    // console.log("params.data", params.data)
   if (params.data !== undefined) {
     // console.log ( "tes",params.dataRes)
     if(params.data['r7'].indexOf('-') == -1 ){
       if(params.data['r7'].indexOf('(0)') == -1 ){
         return {color: 'rgba(48, 208, 220, 1)'};
       } else return {color: '#fff'}
     }
     else return {
       color: 'rgba(252, 112, 115, 1)'
     };
     }
   },
  },
  {headerName: 'R8', field: 'r8', width: 80, cellRenderer: valueToComma,
  cellStyle: function (params) {
    // console.log("params.data", params.data)
   if (params.data !== undefined) {
     // console.log ( "tes",params.dataRes)
     if(params.data['r8'].indexOf('-') == -1 ){
       if(params.data['r8'].indexOf('(0)') == -1 ){
         return {color: 'rgba(48, 208, 220, 1)'};
       } else return {color: '#fff'}
     }
     else return {
       color: 'rgba(252, 112, 115, 1)'
     };
     }
   },
  },
  {headerName: 'R9', field: 'r9', width: 80, cellRenderer: valueToComma,
  cellStyle: function (params) {
    // console.log("params.data", params.data)
   if (params.data !== undefined) {
     // console.log ( "tes",params.dataRes)
     if(params.data['r9'].indexOf('-') == -1 ){
       if(params.data['r9'].indexOf('(0)') == -1 ){
         return {color: 'rgba(48, 208, 220, 1)'};
       } else return {color: '#fff'}
     }
     else return {
       color: 'rgba(252, 112, 115, 1)'
     };
     }
   },
  },
  {headerName: 'R10', field: 'r10', width: 80, cellRenderer: valueToComma,
  cellStyle: function (params) {
    // console.log("params.data", params.data)
   if (params.data !== undefined) {
     // console.log ( "tes",params.dataRes)
     if(params.data['r10'].indexOf('-') == -1 ){
       if(params.data['r10'].indexOf('(0)') == -1 ){
         return {color: 'rgba(48, 208, 220, 1)'};
       } else return {color: '#fff'}
     }
     else return {
       color: 'rgba(252, 112, 115, 1)'
     };
     }
   },
  },
  {headerName: 'R11', field: 'r11', width: 80, cellRenderer: valueToComma,
  cellStyle: function (params) {
    // console.log("params.data", params.data)
   if (params.data !== undefined) {
     // console.log ( "tes",params.dataRes)
     if(params.data['r11'].indexOf('-') == -1 ){
       if(params.data['r11'].indexOf('(0)') == -1 ){
         return {color: 'rgba(48, 208, 220, 1)'};
       } else return {color: '#fff'}
     }
     else return {
       color: 'rgba(252, 112, 115, 1)'
     };
     }
   },
  },    
]

/*Dummy*/
// export const updownColDefs = [
//   {headerName: 'License Name', field: 'License_Name'},
//   {headerName: 'R1', field: 'Region_A'},
//   {headerName: 'R2', field: 'Region_B'},
//   {headerName: 'R3', field: 'Region_C'},
//   {headerName: 'R4', field: 'Region_D'},
//   {headerName: 'R5', field: 'Region_D'},
//   {headerName: 'R6', field: 'Region_D'},
//   {headerName: 'R7', field: 'Region_D'},
//   {headerName: 'R8', field: 'Region_D'},
//   {headerName: 'R9', field: 'Region_D'},
//   {headerName: 'R10', field: 'Region_D'},
//   {headerName: 'R11', field: 'Region_D'},
// ]

// export const tesTable = [
//   {headerName: "ID", field: "id"},
//   {headerName: "Value", field: "value"}
// ]

function valueToComma(params) {
  if(params.value){
    // console.log("bisaa")
    return '<b>' + params.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '</b>' 
  } else {
    // console.log("tidak bisaa",params)
  return };}