import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { selectorSettings, SettingsActionTypes, ActionSettingsTest } from '../../settings/settings.reducer';
import { Action } from 'rxjs/internal/scheduler/Action';

const Server = 'http://10.54.36.49';
// const Server = 'http://10.47.198.153'; //local mas abdur

@Injectable()
export class FeatureService {
  token = sessionStorage.getItem('token')
  constructor(private httpClient: HttpClient, private store: Store<any>) {
    this.store.select(selectorSettings).subscribe(data => {
      this.token = data.token;
    })
  }

  // public table2g(): any {
  //   let str = Server + "/api-license/public/api/egbts?"+this.token;
  //   return this.httpClient.get(str)
  //     .pipe(
  //       map(res => {
  //         return res;
  //       })
  //     );
  // }

  public Newtable2g(): any {
    let str = Server + "/api-license2/public/api/license2g/Table2G";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );
  }

  // public table3g(): any {
  //   let str = Server + "/api-license/public/api/nodeb?"+this.token;
  //   return this.httpClient.get(str)
  //     .pipe(
  //       map(res => {
  //         return res;
  //       })
  //     );
  // }

  public Newtable3g(): any {
    let str = Server + "/api-license2/public/api/license3g/Table3G";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );
  }

  // public table4g(): any {
  //   let str = Server + "/api-license/public/api/enodeb?"+this.token;
  //   return this.httpClient.get(str)
  //     .pipe(
  //       map(res => {
  //         return res;
  //       })
  //     );
  // }

  public Newtable4g(): any {
    let str = Server + "/api-license2/public/api/license4g/Table4G";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );
  }

  public pieMap(): any {
    let str = Server + "/api-license/public/api/chart?"+this.token;
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }



  public licenseChange2g(){
    let str = Server + "/api-license/public/api/egbts/license_changed?"+this.token;
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public licenseChange3g(){
    let str = Server + "/api-license/public/api/nodeb/license_changed?"+this.token;
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  
  public licenseChange4g(){
    let str = Server + "/api-license/public/api/enodeb/license_changed?"+this.token;
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public LicenseRegion2g(){
    let str = Server + "/api-license2/public/api/license2g/TableReg2G";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public LicenseRegion3g(){
    let str = Server + "/api-license2/public/api/license3g/TableReg3G";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public LicenseRegion4g(){
    let str = Server + "/api-license2/public/api/license4g/TableReg4G";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }



  public boaFormTech(){
    let str = Server + "/api-license/public/api/summary_btsonair/technology?"+this.token;
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public boaFormData(){
    let str = Server + "/api-license/public/api/summary_btsonair/detail_data?"+this.token;
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public NodinTable1(){
    let str = Server + "/api-btsonair/public/api/bts_summary_1";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public NodinTable2(){
    let str = Server + "/api-btsonair/public/api/bts_summary_2";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public NodinTable3(){
    let str = Server + "/api-btsonair/public/api/bts_summary_3";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  // public LicenseChart2G(){
  //   let str = Server + "/api-license/public/api/chart/chart_2g";
  //   return this.httpClient.get(str)
  //     .pipe(
  //       map(res => {
  //         return res;
  //       })
  //     );  
  // }

  // public LicenseChart3G(){
  //   let str = Server + "/api-license/public/api/chart/chart_3g";
  //   return this.httpClient.get(str)
  //     .pipe(
  //       map(res => {
  //         return res;
  //       })
  //     );  
  // }

  // public LicenseChart4G(){
  //   let str = Server + "/api-license/public/api/chart/chart_4g";
  //   return this.httpClient.get(str)
  //     .pipe(
  //       map(res => {
  //         return res;
  //       })
  //     );  
  // }

  public NewLicenseChart2G(){
    let str = Server + "/api-license2/public/api/license2g/ChartTRX";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public PowerLicenseChart2G(){
    let str = Server + "/api-license2/public/api/license2g/ChartPOWER";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public NewLicenseChart3G(){
    let str = Server + "/api-license2/public/api/license3g/ChartTRX";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }
  public PowerLicenseChart3G(){
    let str = Server + "/api-license2/public/api/license3g/ChartPOWER";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  public NewLicenseChart4G(){
    let str = Server + "/api-license2/public/api/license4g/ChartTRX";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }
  public PowerLicenseChart4G(){
    let str = Server + "/api-license2/public/api/license4g/ChartPOWER";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }
  public BandLicenseChart4G(){
    let str = Server + "/api-license2/public/api/license4g/ChartBANDWIDTH";
    return this.httpClient.get(str)
      .pipe(
        map(res => {
          return res;
        })
      );  
  }

  // public test(){
  //   let str ="http://10.46.2.63/license/public/test";
  //   this.httpClient.get(str).subscribe(data => {
  //     this.store.dispatch(new ActionSettingsTest({test: data}))
  //   })
  // }
}