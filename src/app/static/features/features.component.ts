import { Component, OnInit, ViewChild } from '@angular/core';

import { environment as env } from '@env/environment';
import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core';
import 'ag-grid-enterprise';
import {FeatureService} from '../provider/feature-service';

@Component({
  selector: 'anms-features',
  template: `
    <ag-table></ag-table>
    <!-- <button mat-button class="button-list" (click)="dialogNodin(dataNodin)" matTooltip="Nodin"
              matTooltipPosition="right">
        <mat-icon>insert_drive_file</mat-icon>&nbsp;&nbsp;&nbsp;
        Nodin
      </button> -->
  `,

  styleUrls: ['./features.component.scss']
})
export class FeaturesComponent implements OnInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;
  versions = env.versions;
  private columnDefs;
  private defaultColDef;
  private rowData;
  private frameworkComponents;
  private sortingOrder;
  private multiSortKey;

  constructor() {
    // featureservice.layer1().subscribe((data: Array<object>) => {
    //   console.log('yeyeye', data);
    //   this.rowData = data;
    // });

    // this.columnDefs = [
    //   this.columnDefs = [
    //     {
    //       field: "NIT1",
    //       width: 100
    //     },
    //     {
    //       field: "Total_KPI",
    //       width: 100
    //     },
    //     {
    //       field: "JenisTabungan",
    //       width: 100
    //     },
        // {
        //   field: "Gender",
        //   width: 90,
        //   cellRenderer: "genderCellRenderer",
        //   cellEditor: "agRichSelectCellEditor",
        //   cellEditorParams: {
        //     values: ["Male", "Female"],
        //     cellRenderer: "genderCellRenderer"
        //   }
        // },
    //     {
    //       field: "Perolehan",
    //       width: 100,
    //     },
    //     {
    //       field: "Bobot",
    //       width: 70,
    //       cellEditor: "agRichSelectCellEditor",
    //       cellEditorParams: {
    //         cellHeight: 50,
    //         values: ["0.04", "0.025","0.02"]
    //       }
    //     },
    //     {
    //       field: "MeetCriteria",
    //       width: 50
    //     },
    //
    //     {
    //       field: "Pengali",
    //       width: 100
    //     },
    //
    //     {
    //       field: "MaxNilai",
    //       width: 100
    //     },
    //
    //     {
    //       field: "CapingNilai",
    //       width: 100
    //     },
    //   ],
    // ];

    // this.rowData = [
    //   {
    //   "name":"aa"
    // }];
  }

  ngOnInit() {}

  openLink(link: string) {
    window.open(link, '_blank');
  }
}
