import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingsComponent } from './settings';

const routes: Routes = [
  // {
  //   path: '',
  //   redirectTo: 'license',
  //   pathMatch: 'full'
  // },
  {
    path: '',
    redirectTo: 'dashboard-license',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: 'dashboard-license'
  }
];

@NgModule({
  // useHash supports github.io demo page, remove in your app
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
