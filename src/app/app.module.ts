import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { SharedModule } from '@app/shared';
import { CoreModule } from '@app/core';

import { SettingsModule } from './settings';
import { StaticModule } from './static';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import {AgGridModule} from 'ag-grid-angular';
import {FeaturesComponent} from './static/features/features.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { downloadNodin, deleteNodin, nodinComponent } from './static/partial/nodin';
import { AgGridTableComponent } from './static/partial/ag-grid-table';
import {AmChartsModule} from '@amcharts/amcharts3-angular';
import { MatGridListModule, MatDialogModule } from '@angular/material';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

@NgModule({
  imports: [
    // angular
    BrowserAnimationsModule,
    BrowserModule,

    // core & shared
    CoreModule,
    SharedModule,

    // features
    StaticModule,
    SettingsModule,
    // downloadNodin,
    // deleteNodin,
    // nodinComponent,
   

    // app
    AppRoutingModule,

    // agGrid
    AgGridModule.withComponents([]),
    FormsModule,
    HttpClientModule,
    // AgGridTableComponent,
    AmChartsModule,
    MatGridListModule ,
    MDBBootstrapModule.forRoot(),
    MatDialogModule
  ],
  declarations: [AppComponent],
  providers: [
    // downloadNodin,
    // deleteNodin,
    // nodinComponent,
  ],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA ]
})
export class AppModule {}
