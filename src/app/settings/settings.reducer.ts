import { Action } from '@ngrx/store';
import { Observable } from 'rxjs';

export const SETTINGS_KEY = 'SETTINGS';

export enum SettingsActionTypes {
  CHANGE_THEME = '[Settings] Change Theme',
  CHANGE_AUTO_NIGHT_AUTO_MODE = '[Settings] Change Auto Night Mode',
  CHANGE_ANIMATIONS_PAGE = '[Settings] Change Animations Page',
  CHANGE_ANIMATIONS_PAGE_DISABLED = '[Settings] Change Animations Page Disabled',
  CHANGE_ANIMATIONS_ELEMENTS = '[Settings] Change Animations Elements',
  PERSIST = '[Settings] Persist',
  TOKEN = '[Settings] Token',
  BTS_CHART = '[Settings] Bts Chart',
  TEST = '[Settings] Test',
  PIEDATA = '[Settings] Pie Data'
}

export class ActionSettingsChangeTheme implements Action {
  readonly type = SettingsActionTypes.CHANGE_THEME;
  constructor(public payload: { theme: string }) {}
}

export class ActionSettingsChangeAutoNightMode implements Action {
  readonly type = SettingsActionTypes.CHANGE_AUTO_NIGHT_AUTO_MODE;
  constructor(public payload: { autoNightMode: boolean }) {}
}

export class ActionSettingsChangeAnimationsPage implements Action {
  readonly type = SettingsActionTypes.CHANGE_ANIMATIONS_PAGE;
  constructor(public payload: { pageAnimations: boolean }) {}
}

export class ActionSettingsChangeAnimationsPageDisabled implements Action {
  readonly type = SettingsActionTypes.CHANGE_ANIMATIONS_PAGE_DISABLED;
  constructor(public payload: { pageAnimationsDisabled: boolean }) {}
}

export class ActionSettingsChangeAnimationsElements implements Action {
  readonly type = SettingsActionTypes.CHANGE_ANIMATIONS_ELEMENTS;
  constructor(public payload: { elementsAnimations: boolean }) {}
}

export class ActionSettingsPersist implements Action {
  readonly type = SettingsActionTypes.PERSIST;
  constructor(public payload: { settings: SettingsState }) {}
}

export class ActionSettingsPieData implements Action {
  readonly type = SettingsActionTypes.PIEDATA;
  constructor(public payload: { pieData: any }) {}
}

export class ActionSettingsToken implements Action {
  readonly type = SettingsActionTypes.TOKEN;
  constructor(public payload: { token: any }) {}
}

export class ActionSettingsBtsChart implements Action {
  readonly type = SettingsActionTypes.BTS_CHART;
  constructor(public payload: { btsChart: any }) {}
}

export class ActionSettingsTest implements Action {
  readonly type = SettingsActionTypes.TEST;
  constructor(public payload: { test: any }) {}
}

export type SettingsActions =
  | ActionSettingsPersist
  | ActionSettingsChangeTheme
  | ActionSettingsChangeAnimationsPage
  | ActionSettingsChangeAnimationsPageDisabled
  | ActionSettingsChangeAnimationsElements
  | ActionSettingsPieData
  | ActionSettingsToken
  | ActionSettingsBtsChart
  | ActionSettingsTest
  | ActionSettingsChangeAutoNightMode;

export const NIGHT_MODE_THEME = 'BLACK-THEME';

export const initialState: SettingsState = {
  theme: 'BLACK-THEME',
  autoNightMode: false,
  pageAnimations: true,
  pageAnimationsDisabled: false,
  elementsAnimations: true,
  pieData: undefined,
  token: "",
  test: undefined,
  btsChart : [{
    "lineColor": "#b7e021",
    "date": "2012-01-01",
    "duration": 408
}, {
    "date": "2012-01-02",
    "duration": 482
}, {
    "date": "2012-01-03",
    "duration": 562
}, {
    "date": "2012-01-04",
    "duration": 379
}, {
    "lineColor": "#fbd51a",
    "date": "2012-01-05",
    "duration": 501
}, {
    "date": "2012-01-06",
    "duration": 443
}, {
    "date": "2012-01-07",
    "duration": 405
}, {
    "date": "2012-01-08",
    "duration": 309,
    "lineColor": "#2498d2"
}, {
    "date": "2012-01-09",
    "duration": 287
}, {
    "date": "2012-01-10",
    "duration": 485
}, {
    "date": "2012-01-11",
    "duration": 890
}, {
    "date": "2012-01-12",
    "duration": 810
}]
};

export const selectorSettings = state =>
  <SettingsState>(state.settings || { theme: '' });

export function settingsReducer(
  state: SettingsState = initialState,
  action: SettingsActions
): SettingsState {
  switch (action.type) {
    case SettingsActionTypes.CHANGE_THEME:
      return { ...state, theme: action.payload.theme };

    case SettingsActionTypes.CHANGE_AUTO_NIGHT_AUTO_MODE:
      return { ...state, autoNightMode: action.payload.autoNightMode };

    case SettingsActionTypes.CHANGE_ANIMATIONS_PAGE:
      return { ...state, pageAnimations: action.payload.pageAnimations };

    case SettingsActionTypes.CHANGE_ANIMATIONS_ELEMENTS:
      return {
        ...state,
        elementsAnimations: action.payload.elementsAnimations
      };

    case SettingsActionTypes.CHANGE_ANIMATIONS_PAGE_DISABLED:
      return {
        ...state,
        pageAnimations: false,
        pageAnimationsDisabled: action.payload.pageAnimationsDisabled
      };

    case SettingsActionTypes.PIEDATA:
      return { ...state, pieData: action.payload.pieData };

    case SettingsActionTypes.TOKEN:
      return { ...state, token: action.payload.token };

    case SettingsActionTypes.BTS_CHART:
      return { ...state, token: action.payload.btsChart };

    case SettingsActionTypes.TEST:
      return { ...state, test: action.payload.test };

    default:
      return state;
  }
}

export interface SettingsState {
  theme: string;
  autoNightMode: boolean;
  pageAnimations: boolean;
  pageAnimationsDisabled: boolean;
  elementsAnimations: boolean;
  pieData: any;
  token: string;
  test: any;
  btsChart: any;
}
