import browser from 'browser-detect';
import { Title } from '@angular/platform-browser';
import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, HostBinding, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivationEnd, Router, NavigationEnd } from '@angular/router';
import { Store } from '@ngrx/store';
import { Subject, Observable } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';

// tambah
// import { MOBILE }

import {
  ActionAuthLogin,
  ActionAuthLogout,
  AnimationsService,
  selectorAuth,
  routeAnimations
} from '@app/core';
import { environment as env } from '@env/environment';

import {
  NIGHT_MODE_THEME,
  selectorSettings,
  SettingsState,
  ActionSettingsChangeAnimationsPageDisabled,
  ActionSettingsToken,
  ActionSettingsTest
} from './settings';
import { FeatureService } from './static/provider/feature-service';
import { HttpClient } from '@angular/common/http';
import { Tes } from './static/model/model';

@Component({
  selector: 'anms-root',
  // templateUrl: './app.component.html',
  templateUrl: './app.component.2.html',
  styleUrls: ['./app.component.scss'],
  animations: [routeAnimations]
})
export class AppComponent implements OnInit, OnDestroy {
  private unsubscribe$: Subject<void> = new Subject<void>();
  private token: string;
  @HostBinding('class') componentCssClass;
  test:any[]=Array();
  tis:any={"value":0};
  isProd = env.production;
  envName = env.envName;
  version = env.versions.app;
  year = new Date().getFullYear();
  logo = require('../assets/logo.png');
  navigation = [
    { link: 'license', label: 'LICENSE' }
  ];
  navigationSideMenu = [
    ...this.navigation
  ];
  isAuthenticated;

  constructor(
    public overlayContainer: OverlayContainer,
    private store: Store<any>,
    private router: Router,
    private titleService: Title,
    private animationService: AnimationsService,
    private featureService: FeatureService,
    private httpClient: HttpClient,
    private ref: ChangeDetectorRef
  ) {
    let tesdata;
    // this.store.select(selectorSettings).subscribe(x => {
    //   tesdata = x.test
    //   this.tis = tesdata
    //   this.ref.markForCheck();
    //   console.log(`tesdata = ${tesdata}`)
    // })     

    // setInterval(() => {
    //   this.getTest()  
    //   console.log(`set tis /10sec => ${this.tis}`)  
    // }, 10000);

    // let a = [1,2,3]
    // console.log(`cek include ${a.includes(0)}`)
  }

  private static trackPageView(event: NavigationEnd) {
    (<any>window).ga('set', 'page', event.urlAfterRedirects);
    (<any>window).ga('send', 'pageview');
  }

  private static isIEorEdge() {
    return ['ie', 'edge'].includes(browser().name);
  }

  ngOnInit(): void {
    // setTimeout(() => {
    //   this.getTest()  
    //   console.log(`set tis /10sec => ${this.tis.value}`)  
    // }, 10000);
   
    this.subscribeToSettings();
    this.subscribeToIsAuthenticated();
    this.subscribeToRouterEvents();
    
    if (sessionStorage.getItem('token')) {
      this.token = sessionStorage.getItem('token')
      // this.store.dispatch(new ActionSettingsToken({token: this.token}))
      console.log("dapat session storage", this.token);
    } else {
      console.log('key dose not exists');
      window.location.replace('http://10.54.36.49/landingPage');
    }    
  }

  dropDead() {
    document.getElementById("dropIcon").classList.toggle('putar');
    document.getElementById("dropId").classList.toggle("show");

    // window.onclick = function(event) {
    //   if (!event.target.matches('.dropbtn')) {
    //     var dropdowns = document.getElementsByClassName("dropClass");
    //     var i;
    //     for (i = 0; i < dropdowns.length; i++) {
    //       var openDropdown = dropdowns[i];
    //       if (openDropdown.classList.contains('show')) {
    //         openDropdown.classList.remove('show');
    //       }
    //     }
    //   }
    // }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onLoginClick() {
    this.store.dispatch(new ActionAuthLogin());    
  }

  onLogoutClick() {
    // this.store.dispatch(new ActionAuthLogout());
    sessionStorage.clear();
    window.location.replace('http://10.54.36.49/landingPage/')
  }

  private subscribeToIsAuthenticated() {
    this.store
      .select(selectorAuth)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(auth => (this.isAuthenticated = auth.isAuthenticated));
  }

  private subscribeToSettings() {
    if (AppComponent.isIEorEdge()) {
      this.store.dispatch(
        new ActionSettingsChangeAnimationsPageDisabled({
          pageAnimationsDisabled: true
        })
      );
    }
    this.store
      .select(selectorSettings)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(settings => {
        this.setTheme(settings);
        this.animationService.updateRouteAnimationType(
          settings.pageAnimations,
          settings.elementsAnimations
        );
      });
  }

  private setTheme(settings: SettingsState) {
    const { theme, autoNightMode } = settings;
    const hours = new Date().getHours();
    const effectiveTheme = (autoNightMode && (hours >= 20 || hours <= 6)
      ? NIGHT_MODE_THEME
      : theme
    ).toLowerCase();
    this.componentCssClass = effectiveTheme;
    const classList = this.overlayContainer.getContainerElement().classList;
    const toRemove = Array.from(classList).filter((item: string) =>
      item.includes('-theme')
    );
    if (toRemove.length) {
      classList.remove(...toRemove);
    }
    classList.add(effectiveTheme);
  }

  private subscribeToRouterEvents() {
    this.router.events
      .pipe(
        filter(
          event =>
            event instanceof ActivationEnd || event instanceof NavigationEnd
        ),
        takeUntil(this.unsubscribe$)
      )
      .subscribe(event => {
        if (event instanceof ActivationEnd) {
          this.setPageTitle(event);
        }

        if (event instanceof NavigationEnd) {
          AppComponent.trackPageView(event);
        }
      });
  }

  private setPageTitle(event: ActivationEnd) {
    let lastChild = event.snapshot;
    while (lastChild.children.length) {
      lastChild = lastChild.children[0];
    }
    const { title } = lastChild.data;
    this.titleService.setTitle(
      title ? `${title}` : env.appName
    );
  }

  navigateBts(){
    // console.log("navi")       
    window.location.replace('http://10.54.36.49/btsonair/');
    // window.location.replace('http://google.com');
  }

  navigateNodin(){
    // console.log("navi")       
    // window.location.replace('http://10.54.36.49/btsonair/');
    window.location.replace('http://10.54.36.49/apk-nodin/index.php/NodinController/');
  }

  navigateChange(){
    // console.log("navi")       
    // window.location.replace('http://10.54.36.49/btsonair/');
    window.location.replace('http://10.54.36.49/change-front/public');
  }

  getTest(){    
    let str ="http://10.46.3.118/license/public/test";
    this.httpClient.get<Tes>(str).subscribe(x =>{
      var dataz = x.value;      
      if(!this.test.includes(dataz)){
        this.test.push(dataz)
      }
      
      this.tis = this.test.length
      // console.log(`test => ${this.test} <=> ${dataz}`)
      console.log(`bleh ${this.test.includes(x)}`)
      console.log(`blah ${this.tis}`)
    })
  }

  t(){
    let str ="http://10.46.3.118/license/public/test";
    this.httpClient.get(str).pipe(x =>
      x
    )
  }
}